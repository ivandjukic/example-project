module.exports = {
  parser: '@typescript-eslint/parser',
  parserOptions: {
    "ecmaVersion": 12,
    "sourceType": "module"
  },
  extends: [
    "eslint:recommended",
    "plugin:@typescript-eslint/recommended"
  ],
  plugins: [
      '@typescript-eslint/eslint-plugin',
      "prettier",
      "sort-imports-es6-autofix",
      "import-quotes",
      "jest",
      "eslint-plugin-double-semi",
  ],
  extends: [
    'plugin:@typescript-eslint/recommended',
    'plugin:prettier/recommended',
  ],
  root: true,
  env: {
    node: true,
    jest: true,
  },
  ignorePatterns: ['.eslintrc.js'],
  rules: {
    "sort-imports-es6-autofix/sort-imports-es6": [2, {
      "ignoreCase": false,
      "ignoreMemberSort": false,
      "memberSyntaxSortOrder": ["none", "all", "multiple", "single"]
    }],
    '@typescript-eslint/interface-name-prefix': 'off',
    '@typescript-eslint/explicit-function-return-type': 'off',
    '@typescript-eslint/explicit-module-boundary-types': 'off',
    '@typescript-eslint/no-explicit-any': 'off',
    "prettier/prettier": "error",
    "no-console": [
      2
    ],
    "import-quotes/import-quotes": [
      1,
      "single"
    ],
    "no-unused-vars": "off",
    "@typescript-eslint/no-unused-vars": "error",
    "consistent-return": "off",
    "import/prefer-default-export": "off"
  },
};
