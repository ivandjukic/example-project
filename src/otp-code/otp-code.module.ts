import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { OtpCodeEntity } from '../entity/otpCode.entity';
import { OtpCodeService } from './otp-code.service';

@Module({
  imports: [TypeOrmModule.forFeature([OtpCodeEntity])],
  providers: [OtpCodeService],
})
export class OtpCodeModule {}
