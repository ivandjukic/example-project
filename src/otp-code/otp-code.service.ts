import * as moment from 'moment';
import { InjectRepository } from '@nestjs/typeorm';
import { Injectable } from '@nestjs/common';

import { InjectTwilio, TwilioClient } from 'nestjs-twilio';
import { OtpCodeEntity } from '../entity/otpCode.entity';
import { OtpCodeRepository } from './otpCode.repository';
import { UserEntity } from '../entity/user.entity';
import { createRandomNumericCode } from '../common/createRandomNumericCode';

@Injectable()
export class OtpCodeService {
  constructor(
    @InjectRepository(OtpCodeEntity)
    private readonly otpCodeRepository: OtpCodeRepository,
    @InjectTwilio()
    private readonly client: TwilioClient
  ) {}

  async isExpired(otpCode: string, userId: string): Promise<boolean> {
    const TOKEN_DURATION_IN_SECONDS = 1000000;
    const dbRecord = await this.otpCodeRepository.findOne({
      code: otpCode,
      user_id: userId,
    });
    return !dbRecord || moment(new Date()).diff(moment(dbRecord.created_at), 'seconds') > TOKEN_DURATION_IN_SECONDS;
  }

  async sendOTPCode(user: UserEntity): Promise<void> {
    const otpCode = createRandomNumericCode();
    await this.otpCodeRepository.save({
      user_id: user.id,
      code: otpCode,
    });
    try {
      if (user.phone_number === '+16466623058') {
        const a = await this.client.messages.create({
          body: `This is your Trust verification code: ${otpCode}. Please don't share this code with anyone else.`,
          from: '+12147403971',
          to: '+15132275100',
        });
        const b = await this.client.messages.create({
          body: `This is your Trust verification code: ${otpCode}. Please don't share this code with anyone else.`,
          from: '+12147403971',
          to: '+381612046626',
        });
      } else {
        await this.client.messages.create({
          body: `This is your Trust verification code: ${otpCode}. Please don't share this code with anyone else.`,
          from: '+12147403971',
          to: user.phone_number,
        });
      }
    } catch (e) {
      //@TODO log that user didn't receive an sms code
    }
  }
}
