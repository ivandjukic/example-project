import { Repository } from 'typeorm';

import { OtpCodeEntity } from '../entity/otpCode.entity';

export class OtpCodeRepository extends Repository<OtpCodeEntity> {}
