import { ApiBearerAuth, ApiBody, ApiConsumes, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Body, Controller, HttpCode, HttpStatus, Post, Req, UploadedFile, UseInterceptors } from '@nestjs/common';
import { Express } from 'express';
import { FileInterceptor } from '@nestjs/platform-express';

import { RequestWithUser } from '../common/interfaces/request-with-user.interface';
import { SwaggerTags } from '../common/enums/swagger-tags.enum';
import { UploadRequestDto } from './dto/upload-request.dto';
import { UploadResponseDto } from './dto/upload-response.dto';
import { UploadService } from './upload.service';

@Controller('uploads')
@ApiTags(SwaggerTags.UPLOAD)
export class UploadController {
  constructor(private readonly uploadService: UploadService) {}

  @Post()
  @ApiOperation({ summary: 'Upload audio file' })
  @ApiBearerAuth()
  @ApiConsumes('multipart/form-data')
  @ApiBody({ type: UploadRequestDto })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Successfully uploaded file',
    type: UploadResponseDto,
  })
  @HttpCode(HttpStatus.OK)
  @UseInterceptors(FileInterceptor('file'))
  async uploadFile(
    @Req() request: RequestWithUser,
    @UploadedFile() payload: Express.Multer.File,
    @Body() body: UploadRequestDto
  ): Promise<UploadResponseDto> {
    return this.uploadService.upload(payload, body.audio_length, request.user.id);
  }
}
