import { ConfigService } from '@nestjs/config';
import { Express } from 'express';
import { InjectRepository } from '@nestjs/typeorm';
import { Injectable, UnprocessableEntityException } from '@nestjs/common';
import { S3 } from 'aws-sdk';
import { v4 } from 'uuid';

import { EnvVariableNameEnum } from '../common/enums/env-variable-name.enum';
import { UploadEntity } from '../entity/upload.entity';
import { UploadRepository } from './upload.repository';
import { UploadResponseDto } from './dto/upload-response.dto';

@Injectable()
export class UploadService {
  constructor(
    @InjectRepository(UploadEntity)
    private readonly uploadRepository: UploadRepository,
    private readonly configService: ConfigService
  ) {}

  async findByUrl(url: string): Promise<UploadEntity> {
    return this.uploadRepository.findOne({
      url,
    });
  }

  async upload(payload: Express.Multer.File, audioLength: number, userId?: string): Promise<UploadResponseDto> {
    const { mimetype, size, buffer, originalname } = payload;
    const MAX_UPLOAD_SIZE = 20 * 1024 * 1024; // 20 MB
    const SUPPORTED_FORMATS = ['audio/mp4', 'audio/vnd.wave', 'audio/x-m4a'];
    const extension = mimetype.split('/')[1] ?? '';

    if (size > MAX_UPLOAD_SIZE) {
      throw new UnprocessableEntityException({
        message: 'File too large',
        max_upload_size: `${MAX_UPLOAD_SIZE / (1024 * 1024)} MB`,
      });
    }
    if (!SUPPORTED_FORMATS.includes(mimetype)) {
      throw new UnprocessableEntityException({
        message: 'Unsupported file format',
        supported_formats: SUPPORTED_FORMATS,
      });
    }
    const s3Client = new S3({
      accessKeyId: this.configService.get<string>(EnvVariableNameEnum.AWS_ACCESS_KEY_ID, ''),
      secretAccessKey: this.configService.get<string>(EnvVariableNameEnum.AWS_SECRET_ACCESS_KEY, ''),
      region: this.configService.get<string>(EnvVariableNameEnum.AWS_REGION, ''),
    });

    const response = await s3Client
      .upload({
        Bucket: this.configService.get<string>(EnvVariableNameEnum.AWS_BUCKET_NAME, ''),
        Key: `test/${v4()}.${extension}`,
        ContentType: mimetype,
        Body: buffer,
        ACL: 'public-read',
      })
      .promise();

    await this.uploadRepository.save({
      user_id: userId,
      name: originalname,
      url: response.Location,
      duration: parseFloat((audioLength / 1000).toFixed(2)), // convert milliseconds to seconds
    });
    const uploadRecord = await this.findByUrl(response.Location);

    return {
      id: uploadRecord.id,
      name: uploadRecord.name,
      url: uploadRecord.url,
      duration: parseFloat((audioLength / 1000).toFixed(2)), // convert milliseconds to seconds
    };
  }
}
