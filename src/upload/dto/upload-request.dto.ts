import { ApiProperty } from '@nestjs/swagger';
import { Express } from 'express';
import { IsNumber } from 'class-validator';
import { Transform } from 'class-transformer';

export class UploadRequestDto {
  @ApiProperty({
    name: 'file',
    type: 'file',
    format: 'binary',
    required: true,
    example: 'Audio file',
  })
  file: Express.Multer.File;

  @ApiProperty({
    name: 'audio_length',
    type: 'string',
    required: true,
    example: '3512',
  })
  @Transform(({ value }) => Number(value))
  @IsNumber()
  audio_length: number;
}
