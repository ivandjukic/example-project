import { Repository } from 'typeorm';

import { UploadEntity } from '../entity/upload.entity';

export class UploadRepository extends Repository<UploadEntity> {}
