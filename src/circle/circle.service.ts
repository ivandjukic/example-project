import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Not } from 'typeorm';

import { CircleDto } from './dto/circle.dto';
import { CircleEntity } from '../entity/circle.entity';
import { CircleMemberEntity } from '../entity/circleMember.entity';
import { CircleMemberRepository } from './circle-member.repository';
import { CircleMembersResponseDto } from './dto/circle-member.dto';
import { CircleRelationsEnum } from '../common/enums/circle-relations.enum';
import { CircleRepository } from './circle.repository';
import { NotificationService } from '../notification/notification.service';
import { PaginationQueryRequestDto } from '../common/dto/pagination-query-request.dto';
import { SearchResponseDto } from './dto/search-response.dto';
import { SearchUsersRequestDto } from './dto/search-users-request.dto';
import { UserEntity } from '../entity/user.entity';
import { UserRepository } from '../user/user.repository';
import { UserService } from '../user/user.service';
import { getPagination } from '../common/getPagination';

@Injectable()
export class CircleService {
  constructor(
    @InjectRepository(CircleEntity)
    private readonly circleRepository: CircleRepository,
    @InjectRepository(CircleMemberEntity)
    private readonly circleMemberRepository: CircleMemberRepository,
    @InjectRepository(UserEntity)
    private readonly userRepository: UserRepository,
    private readonly userService: UserService,
    private readonly notificationService: NotificationService
  ) {}

  async save(useId): Promise<void> {
    await this.circleRepository.save({
      user_id: useId,
    });
  }

  async getByUserId(userId: string, relations: CircleRelationsEnum[] = []): Promise<CircleEntity | null> {
    return this.circleRepository.findOne({ user_id: userId }, { relations });
  }

  async getCircleMemberByCircleIdAndMemberId(circledI: string, memberId: string): Promise<CircleMemberEntity | null> {
    return this.circleMemberRepository.findOne({
      circle_id: circledI,
      member_id: memberId,
    });
  }

  async getCircleMembers(userId: string, pagination: PaginationQueryRequestDto): Promise<CircleMembersResponseDto> {
    const page = Number(pagination?.page ?? 1);
    const perPage = Number(pagination?.per_page ?? 20);
    const circle = await this.getByUserId(userId);
    const circleMembers = await this.circleMemberRepository.find({
      where: [{ circle_id: circle.id }, { member_id: userId, are_both_trusted: false }],
      relations: ['user', 'circle'],
      take: perPage,
      skip: (page - 1) * perPage,
    });

    const circleMembersCount = await this.circleMemberRepository.count({
      where: [{ circle_id: circle.id }, { member_id: userId }],
    });

    const members = await Promise.all(
      circleMembers.map(async (circleMember) => {
        return {
          id: circleMember.id,
          user:
            circleMember.user.id !== userId
              ? circleMember.user
              : ((await this.userService.findById(circleMember.circle.user_id)) as UserEntity),
          trusts_me: circleMember.are_both_trusted || circleMember.user.id === userId,
          is_trusted: circleMember.are_both_trusted || circleMember.user.id !== userId,
        };
      })
    );

    return {
      members,
      pagination: getPagination(page, perPage, circleMembersCount),
    };
  }

  async getAllCircleMembers(userId: string): Promise<UserEntity[]> {
    const circle = await this.getByUserId(userId);
    const circleMembers = await this.circleMemberRepository.find({
      where: [{ circle_id: circle.id }, { member_id: userId, are_both_trusted: false }],
      relations: ['user', 'circle'],
    });
    return Promise.all(
      circleMembers
        // filter only users who trusts me
        .filter((circleMember) => circleMember.are_both_trusted || circleMember.user.id === userId)
        .map(async (circleMember) => {
          return circleMember.user.id !== userId
            ? circleMember.user
            : ((await this.userService.findById(circleMember.circle.user_id)) as UserEntity);
        })
    );
  }

  async getCircleMemberIds(userId: string): Promise<string[]> {
    const circle = await this.getByUserId(userId);
    const circleMembers = await this.circleMemberRepository.find({
      where: [
        { circle_id: circle.id, 'user.id': Not(null) },
        { member_id: userId, are_both_trusted: false, 'user.id': Not(null) },
      ],
      relations: ['user', 'circle'],
    });
    return circleMembers.map((circleMember) => {
      if (circleMember.user.id !== userId) return circleMember.user.id;
      return circleMember.circle.user_id;
    });
  }

  async isUserTrusted(otherUserId: string, userId: string): Promise<boolean> {
    const circle = await this.getByUserId(userId);
    if (!circle) {
      return false;
    }
    const member = await this.circleMemberRepository.findOne({
      member_id: otherUserId,
      circle_id: circle.id,
    });

    return !!member;
  }

  async addToCircle(userid: string, userWhoTrusted: UserEntity): Promise<void> {
    if (userid === userWhoTrusted.id) {
      throw new BadRequestException("can't add itself to the trust circle");
    }
    const circle = await this.getByUserId(userWhoTrusted.id);
    const trustedUser = await this.userService.findById(userid);
    if (!circle) {
      throw new BadRequestException('no circle found');
    }
    if (!trustedUser) {
      throw new BadRequestException('user does not exists');
    }

    if (await this.isUserTrusted(userid, userWhoTrusted.id)) {
      throw new BadRequestException('user already trusted');
    }

    const doesTrustedUserAlreadyTrustsAuthenticatedUser = await this.isUserTrusted(userWhoTrusted.id, userid);

    await this.circleMemberRepository.save({
      circle_id: circle.id,
      member_id: userid,
      are_both_trusted: doesTrustedUserAlreadyTrustsAuthenticatedUser,
    });

    if (trustedUser.device_token) {
      await this.notificationService.userTrustedNotification(userWhoTrusted, trustedUser);
    }

    if (doesTrustedUserAlreadyTrustsAuthenticatedUser) {
      await this.updateAreBothTrust(userWhoTrusted.id, userid, true);
    }
  }

  async updateAreBothTrust(userWhoTrustedId: string, alreadyTrustedUserId: string, isTrusted: boolean): Promise<void> {
    const circle = await this.getByUserId(alreadyTrustedUserId);
    if (!circle) {
      throw new BadRequestException('no circle found');
    }
    await this.circleMemberRepository.update(
      {
        circle_id: circle.id,
        member_id: userWhoTrustedId,
      },
      { are_both_trusted: isTrusted }
    );
  }

  async getCircle(userId: string): Promise<CircleDto> {
    const circle = await this.getByUserId(userId);
    if (!circle) {
      throw new BadRequestException('no circle found');
    }
    const circleMembersCount = await this.circleMemberRepository.count({
      circle_id: circle.id,
    });

    return {
      ...circle,
      count_of_circle_members: circleMembersCount,
    };
  }

  async search(user: UserEntity, query: SearchUsersRequestDto): Promise<SearchResponseDto> {
    const circle = await this.getByUserId(user.id, [CircleRelationsEnum.MEMBERS]);
    if (!circle) throw new BadRequestException('no circle found');

    const page = Number(query?.page ?? 1);
    const perPage = Number(query?.per_page ?? 20);

    const results = await this.userRepository
      .createQueryBuilder('users')
      .addSelect(`CASE WHEN circle_members.id is not null THEN true ELSE false END`, 'is_trusted')
      .leftJoinAndSelect(
        (db) =>
          db
            .select('u.*')
            .from('circle_members', 'circle_members')
            .leftJoin('users', 'u', 'circle_members.member_id = u.id')
            .where('circle_members.circle_id = :circleId', { circleId: circle.id }),
        'circle_members',
        'users.id = circle_members.id'
      )
      .where('users.id != :uid AND (users.first_name LIKE :search OR users.last_name LIKE :search OR users.username LIKE :search)', {
        uid: user.id,
        search: `%${query.search}%`,
      })
      .orderBy('is_trusted', 'DESC')
      .limit(perPage)
      .offset((page - 1) * perPage)
      .getRawMany();

    const count = await this.userRepository
      .createQueryBuilder()
      .where('id != :uid AND (first_name LIKE :search OR last_name LIKE :search OR username LIKE :search)', {
        uid: user.id,
        search: `%${query.search}%`,
      })
      .getCount();
    return {
      members: results.map((result) => {
        return {
          id: result.users_id,
          first_name: result.users_first_name,
          last_name: result.users_last_name,
          username: result.users_username,
          is_trusted: result.is_trusted,
        };
      }),
      pagination: getPagination(page, perPage, count),
    };
  }
}
