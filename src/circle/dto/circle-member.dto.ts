import { ApiProperty } from '@nestjs/swagger';

import { PaginationDto } from '../../common/dto/pagination.dto';

export class CircleMemberUserDto {
  @ApiProperty({
    type: 'string',
    name: 'id',
    required: true,
    example: '8608711d-7aa1-4148-a1f7-c1d2e85df0af',
  })
  id: string;

  @ApiProperty({
    type: 'string',
    name: 'username',
    required: true,
    example: 'john_23',
  })
  username: string;

  @ApiProperty({
    type: 'string',
    name: 'first_name',
    required: true,
    example: 'John',
  })
  first_name: string;

  @ApiProperty({
    type: 'string',
    name: 'last_name',
    required: false,
    example: 'Doe',
  })
  last_name?: string;
}

export class CircleMemberDto {
  @ApiProperty({
    type: 'string',
    name: 'id',
    example: '8608711d-7aa1-4148-a1f7-c1d2e85df0af',
  })
  id: string;

  @ApiProperty({
    type: 'CircleMemberUserDto',
    name: 'user',
    example: {
      id: '8608711d-7aa1-4148-a1f7-c1d2e85df0af',
      first_name: 'John',
      last_name: 'Doe',
    },
  })
  user?: CircleMemberUserDto;

  @ApiProperty({
    type: 'boolean',
    name: 'is_trusted',
    example: true,
  })
  is_trusted: boolean;

  @ApiProperty({
    type: 'boolean',
    name: 'trusts_me',
    example: false,
  })
  trusts_me: boolean;
}

export class CircleMembersResponseDto {
  members: CircleMemberDto[];
  pagination: PaginationDto;
}
