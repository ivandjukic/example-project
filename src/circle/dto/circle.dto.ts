import { ApiProperty } from '@nestjs/swagger';

export class CircleDto {
  @ApiProperty({
    type: 'string',
    name: 'id',
    example: '8608711d-7aa1-4148-a1f7-c1d2e85df0af',
    required: true,
  })
  id: string;

  @ApiProperty({
    type: 'string',
    name: 'user_id',
    example: '8608711d-7aa1-4148-a1f7-c1d2e85df055',
    required: true,
  })
  user_id: string;

  @ApiProperty({
    type: 'date',
    name: 'created_at',
    example: '2020-01-01 15:15:15',
    required: true,
  })
  created_at: Date;

  @ApiProperty({
    type: 'date',
    name: 'updated_at',
    example: '2020-01-01 15:15:15',
    required: true,
  })
  updated_at: Date;

  @ApiProperty({
    type: 'number',
    name: 'count_of_circle_members',
    example: 10,
    required: true,
  })
  count_of_circle_members: number;
}
