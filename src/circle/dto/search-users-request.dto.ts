import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

import { PaginationQueryRequestDto } from '../../common/dto/pagination-query-request.dto';

export class SearchUsersRequestDto extends PaginationQueryRequestDto {
  @ApiProperty({
    name: 'search',
    type: 'string',
    example: 'ivan',
    required: true,
  })
  @IsString()
  search: string;
}
