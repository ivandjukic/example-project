import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class AddToCircleRequestDto {
  @ApiProperty({
    name: 'user_id',
    type: 'string',
    example: '0a9da35a-0f2e-4314-88a3-06c520d60203',
  })
  @IsString()
  user_id: string;
}
