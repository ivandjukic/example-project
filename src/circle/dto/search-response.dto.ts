import { ApiProperty } from '@nestjs/swagger';

import { PaginationDto } from '../../common/dto/pagination.dto';

export class SearchMemberDto {
  @ApiProperty({
    type: 'string',
    name: 'id',
    required: true,
    example: '8608711d-7aa1-4148-a1f7-c1d2e85df0af',
  })
  id: string;

  @ApiProperty({
    type: 'string',
    name: 'username',
    required: true,
    example: 'john_23',
  })
  username: string;

  @ApiProperty({
    type: 'string',
    name: 'first_name',
    required: true,
    example: 'John',
  })
  first_name: string;

  @ApiProperty({
    type: 'string',
    name: 'last_name',
    required: false,
    example: 'Doe',
  })
  last_name?: string;

  @ApiProperty({
    type: 'boolean',
    name: 'is_trusted',
    required: true,
    example: true,
  })
  is_trusted: symbol;
}

export class SearchResponseDto {
  members: SearchMemberDto[];
  pagination: PaginationDto;
}
