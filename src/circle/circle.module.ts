import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { CircleController } from './circle.controller';
import { CircleEntity } from '../entity/circle.entity';
import { CircleMemberEntity } from '../entity/circleMember.entity';
import { CircleService } from './circle.service';
import { InviteEntity } from '../entity/invite.entity';
import { InviteService } from '../invite/invite.service';
import { NotificationService } from '../notification/notification.service';
import { OtpCodeEntity } from '../entity/otpCode.entity';
import { OtpCodeService } from '../otp-code/otp-code.service';
import { SettingsEntity } from '../entity/settings.entity';
import { SettingsService } from '../settings/settings.service';
import { UserEntity } from '../entity/user.entity';
import { UserService } from '../user/user.service';
import { WaitlistEntity } from '../entity/waitlist.entity';
import { WaitlistService } from '../waitlist/waitlist.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([CircleEntity, CircleMemberEntity, UserEntity, SettingsEntity, InviteEntity, OtpCodeEntity, WaitlistEntity]),
  ],
  providers: [CircleService, UserService, SettingsService, InviteService, OtpCodeService, NotificationService, WaitlistService],
  controllers: [CircleController],
})
export class CircleModule {}
