import { ApiBearerAuth, ApiBody, ApiOperation, ApiQuery, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Body, Controller, Get, HttpCode, HttpStatus, Post, Query, Req } from '@nestjs/common';

import { AddToCircleRequestDto } from './dto/add-to-circle-request.dto';
import { CircleDto } from './dto/circle.dto';
import { CircleMembersResponseDto } from './dto/circle-member.dto';
import { CircleService } from './circle.service';
import { PaginationQueryRequestDto } from '../common/dto/pagination-query-request.dto';
import { RequestWithUser } from '../common/interfaces/request-with-user.interface';
import { SearchResponseDto } from './dto/search-response.dto';
import { SearchUsersRequestDto } from './dto/search-users-request.dto';
import { SwaggerTags } from '../common/enums/swagger-tags.enum';

@Controller('circles')
@ApiTags(SwaggerTags.CIRCLE)
export class CircleController {
  constructor(private readonly circleService: CircleService) {}

  @Get()
  @ApiOperation({ summary: 'Get circle' })
  @ApiBearerAuth()
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Successfully retrieved circle',
    type: CircleDto,
    isArray: true,
  })
  @HttpCode(200)
  async getCircle(@Req() request: RequestWithUser): Promise<CircleDto> {
    return this.circleService.getCircle(request.user.id);
  }

  @Get('members')
  @ApiOperation({ summary: 'Get circle members' })
  @ApiBearerAuth()
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Successfully retrieved circle members',
    type: CircleMembersResponseDto,
    isArray: true,
  })
  @ApiQuery({ type: PaginationQueryRequestDto })
  @HttpCode(200)
  async getCircleMembers(@Req() request: RequestWithUser, @Query() query: PaginationQueryRequestDto): Promise<CircleMembersResponseDto> {
    return this.circleService.getCircleMembers(request.user.id, query);
  }

  @Post('members')
  @ApiOperation({ summary: 'Add to circle' })
  @ApiBody({ type: AddToCircleRequestDto })
  @ApiBearerAuth()
  @HttpCode(200)
  async addToCircle(@Req() request: RequestWithUser, @Body() payload: AddToCircleRequestDto) {
    await this.circleService.addToCircle(payload.user_id, request.user);
  }

  @Get('/search')
  @ApiOperation({ summary: 'Search users' })
  @ApiBearerAuth()
  @ApiQuery({ type: SearchUsersRequestDto })
  @HttpCode(200)
  @ApiResponse({
    status: HttpStatus.OK,
    type: SearchResponseDto,
    isArray: true,
  })
  async search(@Req() request: RequestWithUser, @Query() query: SearchUsersRequestDto): Promise<SearchResponseDto> {
    return this.circleService.search(request.user, query);
  }
}
