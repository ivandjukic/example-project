import { Repository } from 'typeorm';

import { CircleMemberEntity } from '../entity/circleMember.entity';

export class CircleMemberRepository extends Repository<CircleMemberEntity> {}
