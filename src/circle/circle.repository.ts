import { Repository } from 'typeorm';

import { CircleEntity } from '../entity/circle.entity';

export class CircleRepository extends Repository<CircleEntity> {}
