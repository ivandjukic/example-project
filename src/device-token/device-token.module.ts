import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { DeviceTokenController } from './device-token.controller';
import { DeviceTokenEntity } from '../entity/device-token.entity';
import { DeviceTokenService } from './device-token.service';

@Module({
  imports: [TypeOrmModule.forFeature([DeviceTokenEntity])],
  providers: [DeviceTokenService],
  controllers: [DeviceTokenController],
})
export class DeviceTokenModule {}
