import { Repository } from 'typeorm';

import { DeviceTokenEntity } from '../entity/device-token.entity';

export class DeviceTokenRepository extends Repository<DeviceTokenEntity> {}
