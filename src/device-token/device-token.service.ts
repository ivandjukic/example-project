import { InjectRepository } from '@nestjs/typeorm';
import { Injectable } from '@nestjs/common';

import { DeviceTokenEntity } from '../entity/device-token.entity';
import { DeviceTokenRepository } from './device-token.repository';

@Injectable()
export class DeviceTokenService {
  constructor(
    @InjectRepository(DeviceTokenEntity)
    private readonly deviceTokenRepository: DeviceTokenRepository
  ) {}

  async save(userId: string, token: string): Promise<void> {
    await this.deviceTokenRepository.upsert({ token, user_id: userId, updated_at: new Date() }, ['user_id']);
  }
}
