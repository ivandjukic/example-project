import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class SaveDeviceTokenRequestDto {
  @ApiProperty({
    type: 'string',
    maxLength: 100,
    name: 'token',
    example: '8608711d7aa14148a1f7c1d2e85df0af',
  })
  @IsString()
  token: string;
}
