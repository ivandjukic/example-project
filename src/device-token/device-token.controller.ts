import { ApiBearerAuth, ApiBody, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Body, Controller, HttpCode, HttpStatus, Post, Req } from '@nestjs/common';

import { DeviceTokenService } from './device-token.service';
import { EmptyResponseDto } from '../common/dto/EmptyResponse.dto';
import { RequestWithUser } from '../common/interfaces/request-with-user.interface';
import { SaveDeviceTokenRequestDto } from './dto/save-device-token-request.dto';
import { SwaggerTags } from '../common/enums/swagger-tags.enum';

@Controller('device-tokens')
@ApiTags(SwaggerTags.DEVICE_TOKEN)
export class DeviceTokenController {
  constructor(private readonly deviceTokenService: DeviceTokenService) {}

  @Post('/')
  @ApiOperation({ summary: 'Save device token' })
  @ApiBody({ type: SaveDeviceTokenRequestDto })
  @ApiBearerAuth()
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Successfully saved device token',
    type: EmptyResponseDto,
  })
  @ApiResponse({ status: HttpStatus.BAD_REQUEST, description: 'token must be a string' })
  @HttpCode(HttpStatus.OK)
  save(@Body() payload: SaveDeviceTokenRequestDto, @Req() request: RequestWithUser): EmptyResponseDto {
    return this.deviceTokenService.save(request.user.id, payload.token);
  }
}
