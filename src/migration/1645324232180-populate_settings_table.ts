import { MigrationInterface, QueryRunner } from 'typeorm';

export class populateSettingsTable1645324232180 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`CREATE EXTENSION IF NOT EXISTS "uuid-ossp";`);
    await queryRunner.query(`INSERT INTO settings VALUES(gen_random_uuid(), 'current_app_version', '0.5');`);
    await queryRunner.query(`INSERT INTO settings VALUES(gen_random_uuid(), 'fcm_api_key', 'fcm_api_key');`);
    await queryRunner.query(`INSERT INTO settings VALUES(gen_random_uuid(), 'latest_supported_app_version', '0.4');`);
    await queryRunner.query(`INSERT INTO settings VALUES(gen_random_uuid(), 'invite_only_signup', '0');`);
    await queryRunner.query(`INSERT INTO settings VALUES(gen_random_uuid(), 'trust_account_id', '');`);
    await queryRunner.query(`INSERT INTO settings VALUES(gen_random_uuid(), 'max_signed_users', '-1');`);
    await queryRunner.query(`INSERT INTO settings VALUES(gen_random_uuid(), 'max_invites_per_user', '5');`);
    await queryRunner.query(
      `INSERT INTO settings VALUES(gen_random_uuid(), 'terms_and_conditions', '<p>The use of your provided information by Trust will be limited by our agreements with registered users. Trust may use provided information to analyze, personalize, and improve services; to respond to questions, concerns, or user requests; and to comply with law and legal processes. Information provided to Trust (including account details, digital photography, media, survey or prompt responses, or any other information you choose to provide) shall not be shared with any third party without your prior consent.');`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DELETE FROM settings`);
  }
}
