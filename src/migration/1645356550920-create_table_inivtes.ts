import { MigrationInterface, QueryRunner } from 'typeorm';

export class createTableInivtes1645356550920 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
        CREATE TABLE IF NOT EXISTS invites (
          id UUID NOT NULL PRIMARY KEY UNIQUE DEFAULT gen_random_uuid(),
          invited_by UUID REFERENCES users (id) NOT NULL,
          phone_number VARCHAR(32) NOT NULL,
          created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
          updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
        );
    `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TABLE IF EXISTS invites`);
  }
}
