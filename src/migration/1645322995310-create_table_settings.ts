import { MigrationInterface, QueryRunner } from 'typeorm';

export class createTableSettings1645322995310 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
      CREATE TABLE IF NOT EXISTS settings (
        id UUID NOT NULL PRIMARY KEY UNIQUE DEFAULT gen_random_uuid(),
        key varchar(255) NOT NULL,
        value text,
        created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
        updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
      );
    `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TABLE IF EXISTS settings`);
  }
}
