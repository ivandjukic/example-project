import { MigrationInterface, QueryRunner } from 'typeorm';

export class alterTableUsersAddIsCreator1650580985633 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
            ALTER TABLE users
            ADD COLUMN is_creator BOOLEAN NOT NULL default false;
      `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
            ALTER TABLE users DROP COLUMN is_creator;
        `);
  }
}
