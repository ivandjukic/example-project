import { MigrationInterface, QueryRunner } from 'typeorm';

export class createTableFeedItems1645828124445 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
        CREATE TABLE IF NOT EXISTS feed_items (
          id UUID NOT NULL PRIMARY KEY UNIQUE DEFAULT gen_random_uuid(),
          title VARCHAR NOT NULL,
          type VARCHAR(10) NOT NULL,
          visibility VARCHAR(10) NOT NULL,
          author_id UUID NOT NULL REFERENCES users (id),
          media_id UUID REFERENCES uploads (id),
          responding_to_feed_item_id UUID REFERENCES feed_items (id),
          created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
          updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
          deleted_at TIMESTAMP
        );
      `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TABLE IF EXISTS feed_items`);
  }
}
