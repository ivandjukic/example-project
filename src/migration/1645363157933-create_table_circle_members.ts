import { MigrationInterface, QueryRunner } from 'typeorm';

export class createTableCircleMembers1645363157933 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
        CREATE TABLE IF NOT EXISTS circle_members (
          id UUID NOT NULL PRIMARY KEY UNIQUE DEFAULT gen_random_uuid(),
          circle_id UUID REFERENCES circles (id),
          member_id UUID REFERENCES users (id),
          are_both_trusted BOOLEAN NOT NULL DEFAULT FALSE,
          created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
          updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
          deleted_at TIMESTAMP
        );
      `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TABLE IF EXISTS circle_members`);
  }
}
