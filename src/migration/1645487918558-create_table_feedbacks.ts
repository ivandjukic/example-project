import { MigrationInterface, QueryRunner } from 'typeorm';

export class createTableFeedbacks1645487918558 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
        CREATE TABLE IF NOT EXISTS feedbacks (
          id UUID NOT NULL PRIMARY KEY UNIQUE DEFAULT gen_random_uuid(),
          user_id UUID REFERENCES users (id) NOT NULL,
          rate SMALLINT NOT NULL,
          feedback TEXT NOT NULL,
          os_type VARCHAR(100),
          os_version VARCHAR(100),
          device VARCHAR(100),
          app_version VARCHAR(100),
          created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
          updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
        );
      `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TABLE IF EXISTS feedbacks`);
  }
}
