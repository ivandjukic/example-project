import { ConfigService } from '@nestjs/config';
import { ForbiddenException, Injectable, UnprocessableEntityException } from '@nestjs/common';
import { sign, verify } from 'jsonwebtoken';

import { CircleService } from '../circle/circle.service';
import { EnvVariableNameEnum } from '../common/enums/env-variable-name.enum';
import { InviteService } from '../invite/invite.service';
import { LoginRequestDto } from './dto/login-request.dto';
import { LoginService } from '../login/login.service';
import { LoginTypeEnum } from '../login/enums/login-type.enum';
import { OtpCodeService } from '../otp-code/otp-code.service';
import { RefreshTokenRequestDto } from './dto/refresh-token-request.dto';
import { RegisterRequestDto } from './dto/register-request.dto';
import { ResendOtpCodeRequestDto } from './dto/resend-otp-code-request.dto';
import { SendLoginOtpCodeRequestDto } from './dto/send-login-otp-code-request.dto';
import { SettingKey } from '../settings/enum/settings-key.enum';
import { SettingsService } from '../settings/settings.service';
import { SuccessfullySingedResponseDto } from './dto/successfully-singed-response.dto';
import { UserEntity } from '../entity/user.entity';
import { UserService } from '../user/user.service';
import { VerifyPhoneNumberRequestDto } from './dto/verify-phone-number-request.dto';
import { isPhoneNumberValid } from '../common/phoneNumberValidator';

@Injectable()
export class AuthenticationService {
  constructor(
    private readonly otpCodeService: OtpCodeService,
    private readonly userService: UserService,
    private readonly circleService: CircleService,
    private readonly inviteService: InviteService,
    private readonly settingsService: SettingsService,
    private readonly configService: ConfigService,
    private readonly loginService: LoginService
  ) {}

  async register(payload: RegisterRequestDto): Promise<void> {
    if (!isPhoneNumberValid(payload.phone_number)) {
      throw new UnprocessableEntityException('Invalid phone number provided');
    }
    if (await this.userService.findByPhoneNumber(payload.phone_number)) {
      throw new UnprocessableEntityException('An account associated with this phone number already exists.');
    }
    if (await this.userService.findByUsername(payload.username)) {
      throw new UnprocessableEntityException('Sorry, that one’s taken - pick another one!');
    }
    const invite = await this.inviteService.getByPhoneNumber(payload.phone_number);
    const settings = await this.settingsService.getByKey(SettingKey.INVITE_ONLY_SIGNUP);

    if (!!Number(settings.value) && !invite) {
      throw new ForbiddenException('You are not invited to the APP.');
    }

    await this.userService.save({
      first_name: payload.first_name,
      last_name: payload.last_name,
      phone_number: payload.phone_number,
      username: payload.username,
    });
    const user = await this.userService.findByPhoneNumber(payload.phone_number);
    await this.circleService.save(user.id);
    if (invite) {
      await this.circleService.addToCircle(user.id, await this.userService.findById(invite.invited_by));
    }

    await this.otpCodeService.sendOTPCode(user);
  }

  async generateAccessToken(userId: string): Promise<string> {
    const token = sign({ id: userId }, this.configService.get<string>(EnvVariableNameEnum.JWT_SECRET, ''), {
      expiresIn: this.configService.get<string>(EnvVariableNameEnum.JWT_TOKEN_EXPIRES_IN, '1800s'),
    });
    return token;
  }

  async generateRefreshToken(userId: string): Promise<string> {
    const token = sign({ id: userId }, this.configService.get<string>(EnvVariableNameEnum.JWT_SECRET, ''), {
      expiresIn: this.configService.get<string>(EnvVariableNameEnum.JWT_REFRESH_TOKEN_EXPIRES_IN, '31d'),
    });
    return token;
  }

  async confirmPhoneNumber(payload: VerifyPhoneNumberRequestDto): Promise<SuccessfullySingedResponseDto> {
    const user = await this.userService.findByPhoneNumber(payload.phone_number);
    if (!user || user.confirmed) {
      throw new ForbiddenException("Hm, that didn't work. Please try again.");
    }
    if (await this.otpCodeService.isExpired(payload.otp_code, user.id)) {
      throw new ForbiddenException("Hm, that didn't work. Please try again.");
    }
    await this.userService.confirmUser(user.id);
    await this.loginService.save(user.id, LoginTypeEnum.VERIFY_PHONE_NUMBER);
    return {
      authenticated: true,
      token: await this.generateAccessToken(user.id),
      refresh_token: await this.generateRefreshToken(user.id),
    };
  }

  async confirmPhoneNumberResendCode(phoneNumber: string): Promise<void> {
    const user = await this.userService.findByPhoneNumber(phoneNumber);
    if (!user) {
      throw new ForbiddenException("Hm, that didn't work. Please try again.");
    }
    await this.otpCodeService.sendOTPCode(user);
  }

  async sendLoginOTPCode(payload: SendLoginOtpCodeRequestDto): Promise<void> {
    const user = await this.userService.findByPhoneNumber(payload.phone_number);
    if (!user) {
      throw new ForbiddenException("Hm, that didn't work. Please try again.");
    }
    if (!user.confirmed) {
      throw new ForbiddenException('User is not confirmed.');
    }
    await this.otpCodeService.sendOTPCode(user);
  }

  async validateLoginOTPCode(payload: LoginRequestDto): Promise<SuccessfullySingedResponseDto> {
    const user = await this.userService.findByPhoneNumber(payload.phone_number);
    if (!user) {
      throw new ForbiddenException("Hm, that didn't work. Please try again.");
    }
    if (await this.otpCodeService.isExpired(payload.otp_code, user.id)) {
      throw new ForbiddenException("Hm, that didn't work. Please try again.");
    }
    await this.loginService.save(user.id, LoginTypeEnum.LOGIN);
    return {
      authenticated: true,
      token: await this.generateAccessToken(user.id),
      refresh_token: await this.generateRefreshToken(user.id),
    };
  }

  async refreshToken(refreshTokenRequestDto: RefreshTokenRequestDto): Promise<SuccessfullySingedResponseDto> {
    let user: UserEntity | null;
    await verify(
      refreshTokenRequestDto.refresh_token,
      this.configService.get<string>(EnvVariableNameEnum.JWT_SECRET, ''),
      async (err, payload) => {
        if (err || payload === undefined) {
          throw new ForbiddenException("Hm, that didn't work. Please try again.");
        }
        user = await this.userService.findById(payload.id);
      }
    );
    if (!user) {
      throw new ForbiddenException("Hm, that didn't work. Please try again.");
    }
    await this.loginService.save(user.id, LoginTypeEnum.REFRESH);
    return {
      authenticated: true,
      token: await this.generateAccessToken(user.id),
      refresh_token: await this.generateRefreshToken(user.id),
    };
  }

  async resendOtpCode(payload: ResendOtpCodeRequestDto): Promise<void> {
    if (!isPhoneNumberValid(payload.phone_number)) {
      throw new UnprocessableEntityException('invalid phone number provided.');
    }
    const user = await this.userService.findByPhoneNumber(payload.phone_number);
    if (!user) {
      throw new UnprocessableEntityException("Hm, that didn't work. Please try again.");
    }
    await this.otpCodeService.sendOTPCode(user);
  }
}
