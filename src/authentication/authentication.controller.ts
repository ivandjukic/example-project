import { ApiBody, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Body, Controller, HttpCode, HttpStatus, Post } from '@nestjs/common';

import { AuthenticationService } from './authentication.service';
import { ConfirmPhoneNumberResendOtpCodeRequestDto } from './dto/confirm-phone-number-resend-otp-code-request.dto';
import { EmptyResponseDto } from '../common/dto/EmptyResponse.dto';
import { LoginRequestDto } from './dto/login-request.dto';
import { RefreshTokenRequestDto } from './dto/refresh-token-request.dto';
import { RegisterRequestDto } from './dto/register-request.dto';
import { ResendOtpCodeRequestDto } from './dto/resend-otp-code-request.dto';
import { SendLoginOtpCodeRequestDto } from './dto/send-login-otp-code-request.dto';
import { SuccessfullySingedResponseDto } from './dto/successfully-singed-response.dto';
import { SwaggerTags } from '../common/enums/swagger-tags.enum';
import { VerifyPhoneNumberRequestDto } from './dto/verify-phone-number-request.dto';

@Controller('authentication')
@ApiTags(SwaggerTags.AUTHENTICATION)
export class AuthenticationController {
  constructor(private readonly authenticationService: AuthenticationService) {}

  @ApiOperation({ summary: 'Register' })
  @ApiBody({ type: RegisterRequestDto })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Successfully registered',
    type: EmptyResponseDto,
  })
  @ApiResponse({
    status: HttpStatus.UNPROCESSABLE_ENTITY,
    description: 'Invalid phone number provided.',
  })
  @ApiResponse({
    status: HttpStatus.UNPROCESSABLE_ENTITY,
    description: 'An account associated with this phone number already exists.',
  })
  @Post('/register')
  @HttpCode(200)
  async register(@Body() payload: RegisterRequestDto) {
    await this.authenticationService.register(payload);
  }

  @ApiOperation({ summary: 'Verify phone number' })
  @ApiBody({ type: VerifyPhoneNumberRequestDto })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Successfully verified phone number',
    type: SuccessfullySingedResponseDto,
  })
  @ApiResponse({
    status: HttpStatus.FORBIDDEN,
    description: "Hm, that didn't work. Please try again.",
  })
  @Post('/verify-phone-number')
  @HttpCode(200)
  async confirmPhoneNumber(@Body() payload: VerifyPhoneNumberRequestDto): Promise<SuccessfullySingedResponseDto> {
    return this.authenticationService.confirmPhoneNumber(payload);
  }

  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Successfully resend OTP code',
    type: SuccessfullySingedResponseDto,
  })
  @ApiBody({ type: ConfirmPhoneNumberResendOtpCodeRequestDto })
  @Post('/verify-phone-number/resend')
  @HttpCode(200)
  async confirmPhoneNumberResendCode(@Body() payload: ConfirmPhoneNumberResendOtpCodeRequestDto) {
    await this.authenticationService.confirmPhoneNumberResendCode(payload.phone_number);
  }

  @ApiOperation({ summary: 'Login with OTP code' })
  @ApiBody({ type: LoginRequestDto })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Successfully logged in',
    type: SuccessfullySingedResponseDto,
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: "Phone number doesn't exists" })
  @Post('/login')
  @HttpCode(200)
  async loginOtpCode(@Body() payload: LoginRequestDto): Promise<SuccessfullySingedResponseDto> {
    return this.authenticationService.validateLoginOTPCode(payload);
  }

  @ApiOperation({ summary: 'Send authentication sms OTP code' })
  @ApiBody({ type: SendLoginOtpCodeRequestDto })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Successfully sent sms OTP code',
    type: EmptyResponseDto,
  })
  @ApiResponse({
    status: HttpStatus.FORBIDDEN,
    description: "Hm, that didn't work. Please try again.",
  })
  @Post('/login/send-otp')
  @HttpCode(HttpStatus.OK)
  async sendLoginOTPCode(@Body() payload: SendLoginOtpCodeRequestDto) {
    return this.authenticationService.sendLoginOTPCode(payload);
  }

  @ApiOperation({ summary: 'Refresh JWT token' })
  @ApiBody({ type: RefreshTokenRequestDto })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Successfully refresh token',
    type: SuccessfullySingedResponseDto,
  })
  @ApiResponse({
    status: HttpStatus.FORBIDDEN,
    description: "Hm, that didn't work. Please try again.",
  })
  @Post('/refresh-token')
  @HttpCode(HttpStatus.OK)
  async refreshToken(@Body() payload: RefreshTokenRequestDto): Promise<SuccessfullySingedResponseDto> {
    return this.authenticationService.refreshToken(payload);
  }

  @ApiOperation({ summary: 'Resend OTP code' })
  @ApiBody({ type: ResendOtpCodeRequestDto })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Successfully resent otp code',
    type: EmptyResponseDto,
  })
  @Post('/resend-otp-code')
  @HttpCode(HttpStatus.OK)
  async resendOtpCode(@Body() payload: ResendOtpCodeRequestDto): Promise<EmptyResponseDto> {
    await this.authenticationService.resendOtpCode(payload);
    return {};
  }
}
