import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { AuthenticationController } from './authentication.controller';
import { AuthenticationService } from './authentication.service';
import { CircleEntity } from '../entity/circle.entity';
import { CircleMemberEntity } from '../entity/circleMember.entity';
import { CircleService } from '../circle/circle.service';
import { InviteEntity } from '../entity/invite.entity';
import { InviteService } from '../invite/invite.service';
import { LoginEntity } from '../entity/login.entity';
import { LoginService } from '../login/login.service';
import { NotificationService } from '../notification/notification.service';
import { OtpCodeEntity } from '../entity/otpCode.entity';
import { OtpCodeService } from '../otp-code/otp-code.service';
import { SettingsEntity } from '../entity/settings.entity';
import { SettingsService } from '../settings/settings.service';
import { UserEntity } from '../entity/user.entity';
import { UserService } from '../user/user.service';
import { WaitlistEntity } from '../entity/waitlist.entity';
import { WaitlistService } from '../waitlist/waitlist.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      InviteEntity,
      UserEntity,
      CircleEntity,
      CircleMemberEntity,
      WaitlistEntity,
      LoginEntity,
      OtpCodeEntity,
      SettingsEntity,
    ]),
  ],
  controllers: [AuthenticationController],
  providers: [
    AuthenticationService,
    OtpCodeService,
    UserService,
    CircleService,
    SettingsService,
    InviteService,
    NotificationService,
    WaitlistService,
    LoginService,
  ],
})
export class AuthenticationModule {}
