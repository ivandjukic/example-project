import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class SendLoginOtpCodeRequestDto {
  @ApiProperty({
    name: 'phone_number',
    type: 'string',
    example: '+16465106465',
  })
  @IsNotEmpty()
  phone_number: string;
}
