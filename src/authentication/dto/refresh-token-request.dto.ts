import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class RefreshTokenRequestDto {
  @ApiProperty({
    name: 'refresh_token',
    type: 'string',
    example:
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjBkYWU1MWM4LWE3ZTItNDFlNS04ZmM2LWI2NjdjNmNkMDdhNSIsImlhdCI6MTY0NTkxOTE1NSwiZXhwIjoxNjQ4NTk3NTU1fQ.B4eMdnumQ59gZHkbWI-p8rWo1DcL7a6KAIqK2l-Upy0',
  })
  @IsString()
  refresh_token: string;
}
