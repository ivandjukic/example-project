import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class LoginRequestDto {
  @ApiProperty({
    name: 'phone_number',
    type: 'string',
    example: '+16465106465',
  })
  @IsNotEmpty()
  phone_number: string;

  @ApiProperty({
    name: 'otp_code',
    type: 'string',
    example: '1234',
  })
  @IsNotEmpty()
  otp_code: string;
}
