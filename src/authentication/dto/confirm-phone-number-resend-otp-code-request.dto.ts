import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class ConfirmPhoneNumberResendOtpCodeRequestDto {
  @ApiProperty({
    name: 'phone_number',
    type: 'string',
    example: '+16465106465',
    required: true,
  })
  @IsString()
  phone_number: string;
}
