import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class VerifyPhoneNumberRequestDto {
  @ApiProperty({
    name: 'phone_number',
    type: 'string',
    example: '+6465106465',
  })
  @IsString()
  @IsNotEmpty()
  phone_number: string;

  @ApiProperty({
    name: 'otp_code',
    type: 'string',
    example: '123456',
  })
  @IsString()
  @IsNotEmpty()
  otp_code: string;
}
