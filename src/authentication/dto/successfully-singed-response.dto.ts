import { ApiProperty } from '@nestjs/swagger';

export class SuccessfullySingedResponseDto {
  @ApiProperty({
    type: 'boolean',
    name: 'authenticated',
    example: true,
  })
  authenticated: boolean;

  @ApiProperty({
    type: 'string',
    name: 'token',
    example: 'asdadadad',
  })
  token: string;

  @ApiProperty({
    type: 'string',
    name: 'refresh_token',
    example: 'asdasdasdasdas',
  })
  refresh_token: string;
}
