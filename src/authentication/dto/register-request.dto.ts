import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional, IsString } from 'class-validator';

export class RegisterRequestDto {
  @ApiProperty({
    name: 'username',
    type: 'string',
    example: 'john_d3',
    maxLength: 16,
  })
  @IsString()
  username: string;

  @ApiProperty({
    name: 'first_name',
    type: 'string',
    example: 'John',
    maxLength: 16,
  })
  @IsString()
  first_name: string;

  @ApiProperty({
    name: 'last_name',
    type: 'string',
    example: 'Doe',
    maxLength: 16,
  })
  @IsString()
  @IsOptional()
  last_name?: string;

  @ApiProperty({
    name: 'phone_number',
    type: 'string',
    example: '+16465106465',
    maxLength: 16,
  })
  @IsString()
  @IsNotEmpty()
  phone_number: string;
}
