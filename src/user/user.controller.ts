import { ApiBearerAuth, ApiBody, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Body, Controller, Get, HttpCode, HttpStatus, Patch, Post, Req } from '@nestjs/common';

import { AuthenticatedUserResponseDto } from './dto/authenticated-user-response.dto';
import { ConfirmAccountDeletionRequestDto } from './dto/confirm-account-deletion-request.dto';
import { PatchUserRequestDto } from './dto/patch-user-request.dto';
import { RequestWithUser } from '../common/interfaces/request-with-user.interface';
import { SwaggerTags } from '../common/enums/swagger-tags.enum';
import { UserService } from './user.service';

@Controller('users')
@ApiTags(SwaggerTags.USER)
export class UserController {
  constructor(private readonly userService: UserService) {}

  @ApiOperation({ summary: 'Get authenticated user' })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Successfully fetched authenticated user',
    type: AuthenticatedUserResponseDto,
  })
  @ApiBearerAuth()
  @Get('/')
  @HttpCode(200)
  async getAuthenticatedUser(@Req() request: RequestWithUser): Promise<AuthenticatedUserResponseDto> {
    return this.userService.getAuthenticatedUser(request.user.id);
  }

  @ApiOperation({ summary: 'Delete account - send OTP code' })
  @ApiBearerAuth()
  @Post('/delete/send-otp')
  @HttpCode(200)
  async deleteAccountSendOtpCode(@Req() request: RequestWithUser) {
    await this.userService.deleteAccountSendOtpCode(request.user);
  }

  @ApiOperation({ summary: 'Delete account - confirm' })
  @ApiBody({ type: ConfirmAccountDeletionRequestDto })
  @ApiBearerAuth()
  @Post('/delete/confirm')
  @HttpCode(200)
  async confirmAccountDeletion(@Req() request: RequestWithUser, @Body() payload: ConfirmAccountDeletionRequestDto) {
    await this.userService.confirmAccountDeletion(request.user, payload.otp_code);
  }

  @ApiOperation({ summary: 'Patch authenticated user' })
  @ApiBody({ type: PatchUserRequestDto })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Successfully patched authenticated user',
    type: AuthenticatedUserResponseDto,
  })
  @ApiBearerAuth()
  @Patch('/')
  @HttpCode(200)
  async patchAuthenticatedUser(@Req() request: RequestWithUser, @Body() payload: PatchUserRequestDto) {
    return this.userService.patchAuthenticatedUser(payload, request.user);
  }
}
