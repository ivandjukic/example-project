import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { InviteEntity } from '../entity/invite.entity';
import { InviteService } from '../invite/invite.service';
import { OtpCodeEntity } from '../entity/otpCode.entity';
import { OtpCodeService } from '../otp-code/otp-code.service';
import { SettingsEntity } from '../entity/settings.entity';
import { SettingsService } from '../settings/settings.service';
import { UserController } from './user.controller';
import { UserEntity } from '../entity/user.entity';
import { UserService } from './user.service';
import { WaitlistEntity } from '../entity/waitlist.entity';
import { WaitlistService } from '../waitlist/waitlist.service';

@Module({
  imports: [TypeOrmModule.forFeature([UserEntity, InviteEntity, SettingsEntity, OtpCodeEntity, WaitlistEntity])],
  providers: [UserService, SettingsService, InviteService, OtpCodeService, WaitlistService],
  controllers: [UserController],
})
export class UserModule {}
