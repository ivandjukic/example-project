import { ForbiddenException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Like } from 'typeorm';

import { AuthenticatedUserResponseDto } from './dto/authenticated-user-response.dto';
import { CreatorsResponseDto } from '../creator/dto/creators-response.dto';
import { GetCreatorsRequestDto } from '../creator/dto/get-creators-request.dto';
import { InviteService } from '../invite/invite.service';
import { OtpCodeService } from '../otp-code/otp-code.service';
import { PatchUserRequestDto } from './dto/patch-user-request.dto';
import { RegisterRequestDto } from '../authentication/dto/register-request.dto';
import { SettingsService } from '../settings/settings.service';
import { UserEntity } from '../entity/user.entity';
import { UserRelationsEnum } from '../common/enums/user-relations.enum';
import { UserRepository } from './user.repository';
import { getPagination } from '../common/getPagination';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(UserEntity)
    private readonly userRepository: UserRepository,
    private readonly settingsService: SettingsService,
    private readonly inviteService: InviteService,
    private readonly otpCodeService: OtpCodeService
  ) {}

  async findById(userId, relations?: UserRelationsEnum[]): Promise<UserEntity | null> {
    return this.userRepository.findOne(
      { id: userId },
      {
        relations,
      }
    );
  }

  async confirmUser(userId: string): Promise<void> {
    await this.userRepository.update({ id: userId }, { confirmed: new Date() });
  }

  async findByPhoneNumber(phoneNumber: string): Promise<UserEntity | null> {
    return this.userRepository.findOne({ phone_number: phoneNumber });
  }

  async findByUsername(username: string): Promise<UserEntity | null> {
    return this.userRepository.findOne({ username });
  }

  async save(payload: RegisterRequestDto): Promise<UserEntity> {
    return this.userRepository.save(payload);
  }

  async getAuthenticatedUser(userId: string): Promise<AuthenticatedUserResponseDto> {
    const user = await this.findById(userId, [UserRelationsEnum.PROFILE_PHOTO]);
    if (!user) {
      throw new NotFoundException();
    }
    return {
      ...user,
      number_of_available_invites: await this.inviteService.getNumberOfAvailableInvitesByUserId(userId),
    } as AuthenticatedUserResponseDto;
  }

  async deleteAccountSendOtpCode(user: UserEntity): Promise<void> {
    await this.otpCodeService.sendOTPCode(user);
  }

  async confirmAccountDeletion(user: UserEntity, otpCode: string): Promise<void> {
    if (await this.otpCodeService.isExpired(otpCode, user.id)) {
      throw new ForbiddenException("Hm, that didn't work. Please try again.");
    }
    await this.userRepository.softDelete({ id: user.id });
    await this.userRepository.update(
      { id: user.id },
      {
        phone_number: `${user.phone_number}_deleted_${Date.now()}`,
        first_name: `${user.first_name}_deleted_${Date.now()}`,
        last_name: `${user.last_name}_deleted_${Date.now()}`,
        username: `${user.username}_deleted_${Date.now()}`,
      }
    );
  }

  async patchAuthenticatedUser(payload: PatchUserRequestDto, user: UserEntity): Promise<void> {
    const existingUser = await this.findByUsername(payload.username);
    if (payload.username && payload.username !== user.username && !!existingUser) {
      throw new ForbiddenException('Sorry, that one’s taken - pick another one!');
    }
    await this.userRepository.update(
      { id: user.id },
      {
        first_name: payload.first_name ?? user.first_name,
        last_name: payload.last_name ?? user.last_name,
        username: payload.username ?? user.username,
      }
    );
  }

  async getCreators(query: GetCreatorsRequestDto, userId: string): Promise<CreatorsResponseDto> {
    const page = Number(query?.page ?? 1);
    const perPage = Number(query?.per_page ?? 20);
    const creators = await this.userRepository
      .createQueryBuilder('users')
      .leftJoinAndSelect('users.profile_photo', 'profile_photo')
      .addSelect(`CASE WHEN listeners.id is not null THEN true ELSE false END`, 'is_listened')
      .leftJoinAndSelect('listeners', 'listeners', `users.id = listeners.creator_id AND listeners.user_id = '${userId}'  `)
      .where('is_creator = true')
      .andWhere('confirmed IS NOT NULL')
      .andWhere([
        { first_name: Like(`%${query?.search ?? ''}%`) },
        { last_name: Like(`%${query?.search ?? ''}%`) },
        { username: Like(`%${query?.search ?? ''}%`) },
      ])
      .orderBy('is_listened', 'DESC')
      .getRawMany();

    const count = await this.userRepository
      .createQueryBuilder('users')
      .where('is_creator = true')
      .andWhere('confirmed IS NOT NULL')
      .andWhere([
        { first_name: Like(`%${query?.search ?? ''}%`) },
        { last_name: Like(`%${query?.search ?? ''}%`) },
        { username: Like(`%${query?.search ?? ''}%`) },
      ])
      .getCount();

    return {
      creators: creators.map((creator) => {
        return {
          id: creator.users_id,
          username: creator.users_username,
          first_name: creator.users_first_name,
          last_name: creator.users_last_name,
          profile_photo: !creator.profile_photo_id
            ? null
            : {
                id: creator.profile_photo_id,
                url: creator.profile_photo_url,
                user_id: creator.profile_photo_user_id,
                created_at: creator.profile_photo_created_at,
                updated_at: creator.profile_photo_updated_at,
              },
          is_listened: creator.is_listened,
        };
      }),
      pagination: getPagination(page, perPage, count),
    };
  }
}
