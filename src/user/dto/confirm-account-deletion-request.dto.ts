import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class ConfirmAccountDeletionRequestDto {
  @ApiProperty({
    name: 'otp_code',
    type: 'string',
    example: '0718',
  })
  @IsString()
  otp_code: string;
}
