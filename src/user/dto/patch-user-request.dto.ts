import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';

export class PatchUserRequestDto {
  @ApiProperty({
    name: 'first_name',
    type: 'string',
    example: 'John',
  })
  @IsString()
  @IsOptional()
  first_name: string;

  @ApiProperty({
    name: 'last_name',
    type: 'string',
    example: 'Doe',
  })
  @IsString()
  @IsOptional()
  last_name: string;

  @ApiProperty({
    name: 'username',
    type: 'string',
    example: 'john_doe123',
  })
  @IsString()
  @IsOptional()
  username: string;
}
