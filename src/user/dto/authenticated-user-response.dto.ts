import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { ProfilePhotoDto } from '../../profile-photo/dto/profile-photo.dto';

export class AuthenticatedUserResponseDto {
  @ApiProperty({
    type: 'string',
    name: 'id',
    example: '8608711d-7aa1-4148-a1f7-c1d2e85df0af',
  })
  id: string;

  @ApiProperty({
    type: 'string',
    name: 'first_name',
    example: 'John',
  })
  first_name: string;

  @ApiProperty({
    type: 'string',
    name: 'last_name',
    example: 'Doe',
  })
  last_name: string;

  @ApiProperty({
    type: 'string',
    name: 'phone_number',
    example: '+6465106465',
  })
  phone_number: string;

  @ApiProperty({
    type: 'date',
    name: 'created_at',
    example: '2020-01-01 15:15:15',
  })
  created_at: Date;

  @ApiProperty({
    type: 'date',
    name: 'updated_at',
    example: '2020-01-01 15:15:15',
  })
  updated_at: Date;

  @ApiPropertyOptional({
    name: 'deleted_at',
    example: '2020-01-01 15:15:15',
  })
  deleted_at: Date;

  @ApiProperty({
    type: 'number',
    name: 'number_of_available_invites',
    example: 10,
  })
  number_of_available_invites: number;

  profile_photo?: ProfilePhotoDto;
}
