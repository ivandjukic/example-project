import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

export class UserDto {
  @ApiProperty({
    type: 'string',
    name: 'id',
    example: '8608711d-7aa1-4148-a1f7-c1d2e85df0af',
    required: true,
  })
  id: string;

  @ApiProperty({
    type: 'string',
    name: 'username',
    example: 'john_23',
    required: true,
  })
  username: string;

  @ApiProperty({
    type: 'string',
    name: 'first_name',
    example: 'John',
    required: true,
  })
  first_name: string;

  @ApiProperty({
    type: 'string',
    name: 'last_name',
    example: 'Doe',
    required: false,
  })
  last_name?: string;

  @ApiProperty({
    type: 'string',
    name: 'phone_number',
    example: '+6465106465',
    required: true,
  })
  phone_number: string;

  @ApiProperty({
    type: 'date',
    name: 'created_at',
    example: '2020-01-01 15:15:15',
    required: true,
  })
  created_at: Date;

  @ApiProperty({
    type: 'date',
    name: 'updated_at',
    example: '2020-01-01 15:15:15',
    required: true,
  })
  updated_at: Date;

  @ApiPropertyOptional({
    name: 'deleted_at',
    example: '2020-01-01 15:15:15',
    required: false,
  })
  deleted_at?: Date;
}
