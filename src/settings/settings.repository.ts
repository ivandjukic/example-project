import { Repository } from 'typeorm';
import { SettingsEntity } from '../entity/settings.entity';

export class SettingsRepository extends Repository<SettingsEntity> {}
