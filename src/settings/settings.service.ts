import { InjectRepository } from '@nestjs/typeorm';
import { Injectable } from '@nestjs/common';

import { SettingKey } from './enum/settings-key.enum';
import { SettingsEntity } from '../entity/settings.entity';
import { SettingsRepository } from './settings.repository';

@Injectable()
export class SettingsService {
  constructor(
    @InjectRepository(SettingsEntity)
    private readonly settingsRepository: SettingsRepository
  ) {}

  async getByKey(keyName: SettingKey): Promise<SettingsEntity | null> {
    return this.settingsRepository.findOne({
      key: keyName,
    });
  }
}
