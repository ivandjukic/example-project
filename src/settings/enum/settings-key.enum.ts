export enum SettingKey {
  TERMS_AND_CONDITIONS = 'terms_and_conditions',
  MAX_INVITES_PER_USER = 'max_invites_per_user',
  LATEST_SUPPORTED_APP_VERSION = 'latest_supported_app_version',
  CURRENT_APP_VERSION = 'current_app_version',
  MAX_SIGNED_USERS = 'max_signed_users',
  TRUST_ACCOUNT_ID = 'trust_account_id',
  LOGIN_OTP_CODE_DURATION_IN_MINUTES = 'login_otp_code_duration_in_minutes',
  INVITE_ONLY_SIGNUP = 'invite_only_signup',
  FCM_API_KEY = 'fcm_api_key',
}
