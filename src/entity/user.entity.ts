import { Column, DeleteDateColumn, Entity, OneToOne, PrimaryColumn } from 'typeorm';

import { DeviceTokenEntity } from './device-token.entity';
import { ProfilePhotoEntity } from './profile-photo.entity';

@Entity({ name: 'users' })
export class UserEntity {
  @PrimaryColumn({
    name: 'id',
    type: 'uuid',
    nullable: false,
    readonly: true,
    unique: true,
  })
  id: string;

  @Column({
    name: 'username',
    length: 100,
    unique: true,
    type: 'varchar',
    nullable: false,
  })
  username: string;

  @Column({
    name: 'phone_number',
    length: 100,
    unique: true,
    type: 'varchar',
    nullable: false,
  })
  phone_number: string;

  @Column({
    name: 'first_name',
    length: 100,
    type: 'varchar',
    nullable: false,
  })
  first_name: string;

  @Column({
    name: 'last_name',
    length: 100,
    type: 'varchar',
    nullable: true,
  })
  last_name?: string;

  @Column({
    name: 'created_at',
    type: 'timestamp',
    nullable: false,
  })
  created_at: Date;

  @Column({
    name: 'updated_at',
    type: 'timestamp',
    nullable: false,
  })
  updated_at: Date;

  @Column({
    name: 'confirmed',
    type: 'timestamp',
    nullable: true,
  })
  confirmed?: Date;

  @DeleteDateColumn({
    name: 'deleted_at',
    type: 'timestamp',
    nullable: true,
  })
  deleted_at?: Date;

  @OneToOne(() => ProfilePhotoEntity, (profilePhotoEntity) => profilePhotoEntity.user, {
    eager: true,
  })
  profile_photo: ProfilePhotoEntity;

  @OneToOne(() => DeviceTokenEntity, (deviceToken) => deviceToken.user, {
    eager: true,
  })
  device_token: DeviceTokenEntity;

  @Column({
    name: 'is_creator',
    type: 'boolean',
    nullable: true,
    default: false,
  })
  is_creator?: boolean;
}
