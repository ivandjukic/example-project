import { Column, Entity, JoinColumn, ManyToOne, PrimaryColumn } from 'typeorm';
import { FeedItemEntity } from './feed-item.entity';
import { UserEntity } from './user.entity';

@Entity({ name: 'listens' })
export class ListenEntity {
  @PrimaryColumn({
    name: 'id',
    type: 'uuid',
    nullable: false,
    readonly: true,
    unique: true,
  })
  id: string;

  @Column({
    name: 'user_id',
    type: 'uuid',
    nullable: false,
  })
  user_id: string;

  @ManyToOne(() => UserEntity)
  @JoinColumn({ name: 'user_id' })
  user: UserEntity;

  @Column({
    name: 'feed_item_id',
    type: 'uuid',
    nullable: false,
  })
  feed_item_id: string;

  @ManyToOne(() => FeedItemEntity)
  @JoinColumn({ name: 'feed_item_id' })
  feed_item: FeedItemEntity;

  @Column({
    name: 'created_at',
    type: 'timestamp',
    default: 'CURRENT_TIMESTAMP',
    nullable: false,
  })
  created_at: Date;

  @Column({
    name: 'updated_at',
    type: 'timestamp',
    default: 'CURRENT_TIMESTAMP',
    nullable: false,
  })
  updated_at: Date;
}
