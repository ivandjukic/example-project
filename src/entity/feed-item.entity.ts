import { Column, DeleteDateColumn, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryColumn } from 'typeorm';

import { FeedItemSeenEntity } from './feed-item-seen.entity';
import { FeedItemShareEntity } from './feed-item-share.entity';
import { FeedItemTypeEnum } from '../feed-item/enum/feed-item-type.enum';
import { FeedItemVisibilityEnum } from '../feed-item/enum/feed-item-visibility.enum';
import { UploadEntity } from './upload.entity';
import { UserEntity } from './user.entity';

@Entity({ name: 'feed_items' })
export class FeedItemEntity {
  @PrimaryColumn({
    name: 'id',
    type: 'uuid',
    nullable: false,
    readonly: true,
    unique: true,
  })
  id: string;

  @Column({
    name: 'author_id',
    type: 'uuid',
    nullable: false,
  })
  author_id: string;

  @ManyToOne(() => UserEntity, {
    eager: true,
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'author_id' })
  author: UserEntity;

  @Column({
    name: 'title',
    type: 'varchar',
    nullable: false,
  })
  title: string;

  @Column({
    name: 'type',
    type: 'varchar',
    nullable: false,
    length: 10,
  })
  type: FeedItemTypeEnum;

  @Column({
    name: 'visibility',
    type: 'varchar',
    nullable: false,
    length: 10,
  })
  visibility: FeedItemVisibilityEnum;

  @Column({
    name: 'media_id',
    type: 'uuid',
    nullable: false,
  })
  media_id: string;

  @ManyToOne(() => UploadEntity, { eager: true })
  @JoinColumn({ name: 'media_id' })
  media: UploadEntity;

  @Column({
    name: 'responding_to_feed_item_id',
    type: 'uuid',
    nullable: true,
  })
  responding_to_feed_item_id?: string;

  @ManyToOne(() => FeedItemEntity)
  @JoinColumn({ name: 'responding_to_feed_item_id' })
  responding_to_feed_item?: FeedItemEntity;

  @OneToMany(() => FeedItemEntity, (feedItem) => feedItem.responding_to_feed_item)
  responding_feed_items: FeedItemEntity[];

  @Column({
    name: 'created_at',
    type: 'timestamp',
    nullable: false,
  })
  created_at: Date;

  @Column({
    name: 'updated_at',
    type: 'timestamp',
    nullable: false,
  })
  updated_at: Date;

  @DeleteDateColumn({
    name: 'deleted_at',
    type: 'timestamp',
    nullable: true,
  })
  deleted_at?: Date;

  @OneToMany(() => FeedItemShareEntity, (feedItemSharedWith) => feedItemSharedWith.feedItem)
  shared_with: FeedItemShareEntity[];

  @OneToMany(() => FeedItemSeenEntity, (feedItemSeen) => feedItemSeen.feedItem, {
    eager: true,
  })
  seens: FeedItemSeenEntity[];
}
