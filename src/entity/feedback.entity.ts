import { Column, Entity, PrimaryColumn } from 'typeorm';

@Entity({ name: 'feedbacks' })
export class FeedbackEntity {
  @PrimaryColumn({
    name: 'id',
    type: 'uuid',
    nullable: false,
    readonly: true,
    unique: true,
  })
  id: string;

  @Column({
    name: 'user_id',
    type: 'uuid',
    nullable: false,
  })
  user_id: string;

  @Column({
    name: 'rate',
    type: 'smallint',
    nullable: false,
  })
  rate: string;

  @Column({
    name: 'feedback',
    type: 'text',
    nullable: false,
  })
  feedback: string;

  @Column({
    name: 'os_type',
    type: 'varchar',
    nullable: false,
    length: 100,
  })
  os_type: string;

  @Column({
    name: 'os_version',
    type: 'varchar',
    nullable: false,
    length: 100,
  })
  os_version: string;

  @Column({
    name: 'device',
    type: 'varchar',
    nullable: false,
    length: 100,
  })
  device: string;

  @Column({
    name: 'app_version',
    type: 'varchar',
    nullable: false,
    length: 100,
  })
  app_version: string;

  @Column({
    name: 'created_at',
    type: 'timestamp',
    nullable: false,
    default: 'CURRENT_TIMESTAMP',
  })
  created_at: Date;

  @Column({
    name: 'updated_at',
    type: 'timestamp',
    nullable: false,
    default: 'CURRENT_TIMESTAMP',
  })
  updated_at: Date;
}
