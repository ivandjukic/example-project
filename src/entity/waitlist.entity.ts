import { Column, Entity, PrimaryColumn } from 'typeorm';

@Entity({ name: 'waitlist' })
export class WaitlistEntity {
  @PrimaryColumn({
    name: 'id',
    type: 'uuid',
    nullable: false,
    readonly: true,
    unique: true,
  })
  id: string;

  @Column({
    name: 'phone_number',
    type: 'varchar',
    length: 32,
    nullable: false,
  })
  phone_number: string;

  @Column({
    name: 'created_at',
    type: 'timestamp',
    default: 'CURRENT_TIMESTAMP',
    nullable: false,
  })
  created_at: Date;

  @Column({
    name: 'updated_at',
    type: 'timestamp',
    default: 'CURRENT_TIMESTAMP',
    nullable: false,
  })
  updated_at: Date;
}
