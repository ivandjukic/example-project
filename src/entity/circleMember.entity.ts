import { Column, Entity, JoinColumn, ManyToOne, OneToOne, PrimaryColumn } from 'typeorm';

import { CircleEntity } from './circle.entity';
import { UserEntity } from './user.entity';

@Entity({ name: 'circle_members' })
export class CircleMemberEntity {
  @PrimaryColumn({
    name: 'id',
    type: 'uuid',
    nullable: false,
    readonly: true,
    unique: true,
  })
  id: string;

  @Column({
    name: 'circle_id',
    type: 'uuid',
    nullable: false,
  })
  circle_id: string;

  @ManyToOne(() => CircleEntity)
  @JoinColumn({ name: 'circle_id' })
  circle: CircleEntity;

  @Column({
    name: 'member_id',
    type: 'uuid',
    nullable: false,
  })
  member_id: string;

  @OneToOne(() => UserEntity)
  @JoinColumn({ name: 'member_id' })
  user: UserEntity;

  @Column({
    name: 'are_both_trusted',
    type: 'boolean',
    nullable: false,
    default: false,
  })
  are_both_trusted: boolean;

  @Column({
    name: 'created_at',
    type: 'timestamp',
    nullable: false,
  })
  created_at: Date;

  @Column({
    name: 'updated_at',
    type: 'timestamp',
    nullable: false,
  })
  updated_at: Date;

  @Column({
    name: 'deleted_at',
    type: 'timestamp',
    nullable: true,
  })
  deleted_at: Date;
}
