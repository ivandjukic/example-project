import { Column, Entity, JoinColumn, ManyToOne, PrimaryColumn } from 'typeorm';
import { LoginTypeEnum } from '../login/enums/login-type.enum';
import { UserEntity } from './user.entity';

@Entity({ name: 'logins' })
export class LoginEntity {
  @PrimaryColumn({
    name: 'id',
    type: 'uuid',
    nullable: false,
    readonly: true,
    unique: true,
  })
  id: string;

  @Column({
    name: 'user_id',
    type: 'uuid',
    nullable: false,
  })
  user_id: string;

  @ManyToOne(() => UserEntity)
  @JoinColumn({ name: 'user_id' })
  user: UserEntity;

  @Column({
    name: 'type',
    type: 'varchar',
    length: 20,
    nullable: false,
  })
  type: LoginTypeEnum;

  @Column({
    name: 'created_at',
    type: 'timestamp',
    default: 'CURRENT_TIMESTAMP',
    nullable: false,
  })
  created_at: Date;

  @Column({
    name: 'updated_at',
    type: 'timestamp',
    default: 'CURRENT_TIMESTAMP',
    nullable: false,
  })
  updated_at: Date;
}
