import { Column, Entity, OneToMany, PrimaryColumn } from 'typeorm';

import { CircleMemberEntity } from './circleMember.entity';

@Entity({ name: 'circles' })
export class CircleEntity {
  @PrimaryColumn({
    name: 'id',
    type: 'uuid',
    nullable: false,
    readonly: true,
    unique: true,
  })
  id: string;

  @Column({
    name: 'user_id',
    type: 'uuid',
    nullable: false,
  })
  user_id: string;

  @Column({
    name: 'created_at',
    type: 'timestamp',
    nullable: false,
  })
  created_at: Date;

  @Column({
    name: 'updated_at',
    type: 'timestamp',
    nullable: false,
  })
  updated_at: Date;

  @OneToMany(() => CircleMemberEntity, (circleMember) => circleMember.circle)
  members: CircleMemberEntity[];
}
