import { Column, Entity, JoinColumn, ManyToOne, PrimaryColumn } from 'typeorm';

import { UserEntity } from './user.entity';

@Entity({ name: 'invites' })
export class InviteEntity {
  @PrimaryColumn({
    name: 'id',
    type: 'uuid',
    nullable: false,
    readonly: true,
    unique: true,
  })
  id: string;

  @Column({
    name: 'invited_by',
    type: 'uuid',
    nullable: false,
  })
  invited_by: string;

  @ManyToOne(() => UserEntity, { eager: true })
  @JoinColumn({ name: 'invited_by' })
  invited_by_user: UserEntity;

  @Column({
    name: 'phone_number',
    length: 32,
    unique: true,
    type: 'varchar',
    nullable: false,
  })
  phone_number: string;

  @Column({
    name: 'created_at',
    type: 'timestamp',
    nullable: false,
  })
  created_at: Date;

  @Column({
    name: 'updated_at',
    type: 'timestamp',
    nullable: false,
  })
  updated_at: Date;
}
