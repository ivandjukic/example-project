import { Column, Entity, PrimaryColumn } from 'typeorm';

@Entity({ name: 'uploads' })
export class UploadEntity {
  @PrimaryColumn({
    name: 'id',
    type: 'uuid',
    nullable: false,
    readonly: true,
    unique: true,
  })
  id: string;

  @Column({
    name: 'user_id',
    type: 'uuid',
    nullable: false,
  })
  user_id: string;

  @Column({
    name: 'url',
    type: 'varchar',
    nullable: false,
    length: 255,
    unique: true,
  })
  url: string;

  @Column({
    name: 'name',
    type: 'varchar',
    nullable: false,
    length: 255,
  })
  name: string;

  @Column({
    name: 'duration',
    type: 'decimal',
    nullable: false,
    default: 0,
  })
  duration: number;

  @Column({
    name: 'created_at',
    type: 'timestamp',
    nullable: false,
    default: 'CURRENT_TIMESTAMP',
  })
  created_at: Date;

  @Column({
    name: 'updated_at',
    type: 'timestamp',
    nullable: false,
    default: 'CURRENT_TIMESTAMP',
  })
  updated_at: Date;
}
