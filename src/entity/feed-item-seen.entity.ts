import { Column, Entity, JoinColumn, ManyToOne, PrimaryColumn } from 'typeorm';

import { FeedItemEntity } from './feed-item.entity';
import { UserEntity } from './user.entity';

@Entity('feed_item_seens', {
  orderBy: {
    created_at: 'ASC',
  },
})
export class FeedItemSeenEntity {
  @PrimaryColumn({
    name: 'id',
    type: 'uuid',
    nullable: false,
    readonly: true,
    unique: true,
  })
  id: string;

  @Column({
    name: 'feed_item_id',
    type: 'uuid',
    nullable: false,
  })
  feed_item_id: string;

  @ManyToOne(() => FeedItemEntity)
  @JoinColumn({ name: 'feed_item_id' })
  feedItem: FeedItemEntity;

  @Column({
    name: 'user_id',
    type: 'uuid',
    nullable: false,
  })
  user_id: string;

  @ManyToOne(() => UserEntity)
  @JoinColumn({ name: 'user_id' })
  user: UserEntity;

  @Column({
    name: 'created_at',
    type: 'timestamp',
    nullable: false,
  })
  created_at: Date;

  @Column({
    name: 'updated_at',
    type: 'timestamp',
    nullable: false,
  })
  updated_at: Date;
}
