import { Column, Entity, PrimaryColumn } from 'typeorm';

@Entity({ name: 'settings' })
export class SettingsEntity {
  @PrimaryColumn({
    name: 'id',
    type: 'uuid',
    nullable: false,
    readonly: true,
    unique: true,
  })
  id: string;

  @Column({
    name: 'key',
    type: 'varchar',
    nullable: false,
    readonly: true,
  })
  key: string;

  @Column({
    name: 'value',
    type: 'text',
    nullable: false,
    readonly: true,
  })
  value: string;

  @Column({
    name: 'created_at',
    type: 'timestamp',
    nullable: false,
  })
  created_at: Date;

  @Column({
    name: 'updated_at',
    type: 'timestamp',
    nullable: false,
  })
  updated_at: Date;
}
