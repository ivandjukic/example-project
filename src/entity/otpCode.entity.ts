import { Column, Entity, PrimaryColumn } from 'typeorm';

@Entity({ name: 'otp_codes' })
export class OtpCodeEntity {
  @PrimaryColumn({
    name: 'id',
    type: 'uuid',
    nullable: false,
    readonly: true,
    unique: true,
  })
  id: string;

  @Column({
    name: 'code',
    length: 8,
    type: 'varchar',
    nullable: false,
  })
  code: string;

  @Column({
    name: 'user_id',
    type: 'uuid',
    nullable: true,
  })
  user_id: string;

  @Column({
    name: 'created_at',
    type: 'timestamp',
    nullable: false,
  })
  created_at: Date;

  @Column({
    name: 'updated_at',
    type: 'timestamp',
    nullable: false,
  })
  updated_at: Date;
}
