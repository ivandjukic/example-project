import { Controller, Get, HttpCode, HttpStatus, Query, Req } from '@nestjs/common';

import { ApiBearerAuth, ApiOperation, ApiQuery, ApiResponse, ApiTags } from '@nestjs/swagger';
import { CreatorService } from './creator.service';
import { CreatorsResponseDto } from './dto/creators-response.dto';
import { EmptyResponseDto } from '../common/dto/EmptyResponse.dto';
import { GetCreatorsRequestDto } from './dto/get-creators-request.dto';
import { RequestWithUser } from '../common/interfaces/request-with-user.interface';
import { SwaggerTags } from '../common/enums/swagger-tags.enum';

@Controller('creators')
@ApiTags(SwaggerTags.CREATOR)
export class CreatorController {
  constructor(private readonly creatorService: CreatorService) {}

  @ApiOperation({ summary: 'List creator' })
  @ApiBearerAuth()
  @ApiQuery({ type: GetCreatorsRequestDto })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Successfully fetched list of creators',
    type: CreatorsResponseDto,
  })
  @Get('/')
  @HttpCode(HttpStatus.OK)
  async getCreators(@Req() request: RequestWithUser, @Query() query: GetCreatorsRequestDto): Promise<CreatorsResponseDto> {
    return await this.creatorService.getCreators(query, request.user.id);
  }
}
