import { ApiProperty } from '@nestjs/swagger';
import { ProfilePhotoDto } from '../../profile-photo/dto/profile-photo.dto';

export class CreatorDto {
  @ApiProperty({
    type: 'string',
    name: 'id',
    example: '8608711d-7aa1-4148-a1f7-c1d2e85df0af',
    required: true,
  })
  id: string;

  @ApiProperty({
    type: 'string',
    name: 'username',
    example: 'ivan_djukic',
    required: true,
  })
  username: string;

  @ApiProperty({
    type: 'string',
    name: 'first_name',
    example: 'ivan',
    required: true,
  })
  first_name: string;

  @ApiProperty({
    type: 'string',
    name: 'last_name',
    example: 'djukic',
    required: true,
  })
  last_name: string;

  @ApiProperty({
    type: 'boolean',
    name: 'is_listened',
    example: false,
    required: true,
  })
  is_listened: boolean;

  profile_photo: ProfilePhotoDto | null;
}
