import { CreatorDto } from './creator.dto';
import { PaginationDto } from '../../common/dto/pagination.dto';

export class CreatorsResponseDto {
  creators: CreatorDto[];
  pagination: PaginationDto;
}
