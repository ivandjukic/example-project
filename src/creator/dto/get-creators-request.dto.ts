import { ApiProperty } from '@nestjs/swagger';

import { PaginationQueryRequestDto } from '../../common/dto/pagination-query-request.dto';

export class GetCreatorsRequestDto extends PaginationQueryRequestDto {
  @ApiProperty({
    type: 'string',
    name: 'search',
    example: 'trust',
  })
  search: string;
}
