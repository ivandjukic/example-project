import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { CreatorController } from './creator.controller';
import { CreatorService } from './creator.service';
import { InviteEntity } from '../entity/invite.entity';
import { InviteService } from '../invite/invite.service';
import { OtpCodeEntity } from '../entity/otpCode.entity';
import { OtpCodeService } from '../otp-code/otp-code.service';
import { SettingsEntity } from '../entity/settings.entity';
import { SettingsService } from '../settings/settings.service';
import { UserEntity } from '../entity/user.entity';
import { UserService } from '../user/user.service';
import { WaitlistEntity } from '../entity/waitlist.entity';
import { WaitlistService } from '../waitlist/waitlist.service';

@Module({
  imports: [TypeOrmModule.forFeature([UserEntity, SettingsEntity, InviteEntity, OtpCodeEntity, WaitlistEntity])],
  controllers: [CreatorController],
  providers: [CreatorService, UserService, SettingsService, InviteService, OtpCodeService, WaitlistService],
})
export class CreatorModule {}
