import { Injectable } from '@nestjs/common';

import { CreatorsResponseDto } from './dto/creators-response.dto';
import { GetCreatorsRequestDto } from './dto/get-creators-request.dto';
import { UserService } from '../user/user.service';

@Injectable()
export class CreatorService {
  constructor(private readonly userService: UserService) {}

  async getCreators(query: GetCreatorsRequestDto, userId: string): Promise<CreatorsResponseDto> {
    return this.userService.getCreators(query, userId);
  }
}
