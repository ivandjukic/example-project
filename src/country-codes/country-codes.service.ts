import { Country, countries } from 'country-data';
import { Injectable } from '@nestjs/common';

import { CountryCodeDto } from './dto/country-code.dto';

@Injectable()
export class CountryCodesService {
  private filterCountry(country: Country): CountryCodeDto {
    switch (country.name) {
      case 'Dominican Republic':
        return {
          name: country.name,
          icon: country.emoji.trim(),
          code: '+1',
        };
      case 'Kazakhstan':
        return {
          name: country.name,
          icon: country.emoji.trim(),
          code: '+7',
        };
      case 'Puerto Rico':
        return {
          name: country.name,
          icon: country.emoji.trim(),
          code: '+1',
        };
      case 'Russian Federation':
        return {
          name: country.name,
          icon: country.emoji.trim(),
          code: '+7',
        };
      case 'Vatican City State':
        return {
          name: country.name,
          icon: country.emoji.trim(),
          code: '+39',
        };
      default:
        return {
          name: country.name,
          icon: country.emoji.trim(),
          code: country.countryCallingCodes.length ? country.countryCallingCodes[0] : '',
        };
    }
  }

  async getCountryCodes(): Promise<CountryCodeDto[]> {
    return countries.all
      .filter((country: Country) => country.status === 'assigned')
      .map((country: Country) => this.filterCountry(country))
      .sort((country) => (['United States', 'Canada', 'United Kingdom', 'Australia'].includes(country.name) ? -1 : 1));
  }
}
