import { Module } from '@nestjs/common';

import { CountryCodesController } from './country-codes.controller';
import { CountryCodesService } from './country-codes.service';

@Module({
  providers: [CountryCodesService],
  controllers: [CountryCodesController],
})
export class CountryCodesModule {}
