import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Controller, Get, HttpStatus } from '@nestjs/common';

import { CountryCodeDto } from './dto/country-code.dto';
import { CountryCodesService } from './country-codes.service';
import { SwaggerTags } from '../common/enums/swagger-tags.enum';

@Controller('country-codes')
@ApiTags(SwaggerTags.COUNTRY_CODES)
export class CountryCodesController {
  constructor(private readonly countryCodesService: CountryCodesService) {}

  @Get()
  @ApiOperation({ summary: 'Get list of country codes' })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Successfully retrieved list of country codes',
    type: CountryCodeDto,
    isArray: true,
  })
  async getCountryCodes(): Promise<CountryCodeDto[]> {
    return this.countryCodesService.getCountryCodes();
  }
}
