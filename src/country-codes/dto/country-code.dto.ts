import { ApiProperty } from '@nestjs/swagger';

export class CountryCodeDto {
  @ApiProperty({
    type: 'string',
    name: 'name',
    example: 'Estonia',
  })
  name: string;

  @ApiProperty({
    type: 'string',
    name: 'icon',
    example: '🇪🇪',
  })
  icon: string;

  @ApiProperty({
    type: 'string',
    name: 'code',
    example: '+372',
  })
  code: string;
}
