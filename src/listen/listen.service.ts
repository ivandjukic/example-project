import { InjectRepository } from '@nestjs/typeorm';
import { Injectable } from '@nestjs/common';

import { ListenEntity } from '../entity/listen.entity';
import { ListenRepository } from './listen.repository';

@Injectable()
export class ListenService {
  constructor(
    @InjectRepository(ListenEntity)
    private readonly listenRepository: ListenRepository
  ) {}

  async save(feedItemId: string, userId: string): Promise<ListenEntity> {
    return this.listenRepository.save({
      feed_item_id: feedItemId,
      user_id: userId,
    });
  }
}
