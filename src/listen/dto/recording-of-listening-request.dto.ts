import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class RecordingOfListeningRequestDto {
  @ApiProperty({
    name: 'feed_item_id',
    type: 'string',
    example: '0dc4f28e-44c4-4202-afa6-b63e24623f55',
    required: true,
  })
  @IsString()
  feed_item_id: string;
}
