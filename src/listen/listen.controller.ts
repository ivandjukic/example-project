import { ApiBearerAuth, ApiBody, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Body, Controller, HttpCode, HttpStatus, Post, Req } from '@nestjs/common';

import { EmptyResponseDto } from '../common/dto/EmptyResponse.dto';
import { ListenService } from './listen.service';
import { RecordingOfListeningRequestDto } from './dto/recording-of-listening-request.dto';
import { RequestWithUser } from '../common/interfaces/request-with-user.interface';
import { SwaggerTags } from '../common/enums/swagger-tags.enum';

@Controller('listens')
@ApiTags(SwaggerTags.LISTEN)
export class ListenController {
  constructor(private readonly listenService: ListenService) {}

  @ApiOperation({ summary: 'Record listening' })
  @ApiBody({ type: RecordingOfListeningRequestDto })
  @ApiBearerAuth()
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Successfully saved',
    type: EmptyResponseDto,
  })
  @Post('/')
  @HttpCode(HttpStatus.OK)
  async invite(@Req() request: RequestWithUser, @Body() payload: RecordingOfListeningRequestDto): Promise<EmptyResponseDto> {
    await this.listenService.save(payload.feed_item_id, request.user.id);
    return {};
  }
}
