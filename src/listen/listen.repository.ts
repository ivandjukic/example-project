import { Repository } from 'typeorm';

import { ListenEntity } from '../entity/listen.entity';

export class ListenRepository extends Repository<ListenEntity> {}
