import { ListenController } from './listen.controller';
import { ListenEntity } from '../entity/listen.entity';
import { ListenService } from './listen.service';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([ListenEntity])],
  providers: [ListenService],
  controllers: [ListenController],
})
export class ListenModule {}
