import { ApiProperty } from '@nestjs/swagger';

export class InviteDto {
  @ApiProperty({
    type: 'string',
    name: 'id',
    example: 'c08336a7-6174-484c-9e55-f96aeb10493d',
  })
  id: string;

  @ApiProperty({
    type: 'string',
    name: 'phone_number',
    example: '+381612046626',
  })
  phone_number: string;

  @ApiProperty({
    type: 'string',
    name: 'created_at',
    example: '2022-02-22 21:29:08',
  })
  created_at: Date;

  @ApiProperty({
    type: 'string',
    name: 'updated_at',
    example: '2022-02-22 21:29:08',
  })
  updated_at: Date;
}
