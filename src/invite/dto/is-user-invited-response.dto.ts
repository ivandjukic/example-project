import { ApiProperty } from '@nestjs/swagger';

export class IsUserInvitedResponseDto {
  @ApiProperty({
    type: 'boolean',
    name: 'is_invited',
    example: true,
  })
  is_invited: boolean;

  @ApiProperty({
    type: 'boolean',
    name: 'is_on_waitlist',
    example: true,
  })
  is_on_waitlist: boolean;
}
