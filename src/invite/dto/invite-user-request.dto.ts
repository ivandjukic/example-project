import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class InviteUserRequestDto {
  @ApiProperty({
    name: 'phone_number',
    type: 'string',
    maxLength: 32,
    example: '381612046626',
  })
  @IsString()
  phone_number: string;
}
