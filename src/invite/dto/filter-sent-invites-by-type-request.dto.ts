import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsOptional } from 'class-validator';

import { InviteTypesEnum } from '../enums/invite-types.enum';

export class FilterSentInvitesByTypeRequestDto {
  @ApiProperty({
    name: 'type',
    type: 'string',
    example: InviteTypesEnum.PENDING,
    required: false,
  })
  @IsEnum(InviteTypesEnum)
  @IsOptional()
  type: InviteTypesEnum;
}
