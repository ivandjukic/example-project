import { ApiBearerAuth, ApiBody, ApiOperation, ApiQuery, ApiResponse, ApiTags } from '@nestjs/swagger';
import { ApiImplicitParam } from '@nestjs/swagger/dist/decorators/api-implicit-param.decorator';
import { Body, Controller, Get, HttpCode, HttpStatus, Param, Post, Query, Req, Res } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Request, Response } from 'express';

import { EmptyResponseDto } from '../common/dto/EmptyResponse.dto';
import { EnvVariableNameEnum } from '../common/enums/env-variable-name.enum';
import { FilterSentInvitesByTypeRequestDto } from './dto/filter-sent-invites-by-type-request.dto';
import { InviteDto } from './dto/invite.dto';
import { InviteService } from './invite.service';
import { InviteUserRequestDto } from './dto/invite-user-request.dto';
import { IsUserInvitedResponseDto } from './dto/is-user-invited-response.dto';
import { RequestWithUser } from '../common/interfaces/request-with-user.interface';
import { SwaggerTags } from '../common/enums/swagger-tags.enum';

@Controller('invites')
@ApiTags(SwaggerTags.INVITE)
export class InviteController {
  constructor(private readonly inviteService: InviteService, private readonly configService: ConfigService) {}

  @ApiOperation({ summary: 'Invite a user' })
  @ApiBody({ type: InviteUserRequestDto })
  @ApiBearerAuth()
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Successfully returned result',
    type: EmptyResponseDto,
  })
  @Post('/')
  @HttpCode(HttpStatus.OK)
  async invite(@Req() request: RequestWithUser, @Body() payload: InviteUserRequestDto): Promise<EmptyResponseDto> {
    await this.inviteService.invite(payload.phone_number, request.user);
    return {};
  }

  @ApiOperation({ summary: 'Get sent invites' })
  @ApiBearerAuth()
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Successfully returned result',
    type: InviteDto,
    isArray: true,
  })
  @ApiQuery({ type: FilterSentInvitesByTypeRequestDto })
  @Get('/')
  @HttpCode(HttpStatus.OK)
  async getSentInvites(@Req() request: RequestWithUser, @Query() query: FilterSentInvitesByTypeRequestDto): Promise<InviteDto[]> {
    return this.inviteService.getSentInvitesByUserId(request.user.id, query.type);
  }

  @ApiOperation({ summary: 'Is phone number invited' })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Successfully returned result',
    type: IsUserInvitedResponseDto,
  })
  @ApiImplicitParam({
    name: 'phone_number',
    type: 'string',
    required: true,
    example: '+381612046626',
  })
  @Get('/:phone_number/is-invited')
  @HttpCode(HttpStatus.OK)
  async isPhoneNumberInvited(@Param('phone_number') phoneNumber: string): Promise<IsUserInvitedResponseDto> {
    return this.inviteService.isPhoneNumberInvited(phoneNumber);
  }

  @ApiOperation({ summary: 'Accept invite' })
  @Get('/accept')
  @HttpCode(HttpStatus.OK)
  accept(@Req() request: Request, @Res() response: Response) {
    const redirectUrl = request.headers['user-agent'].toLowerCase().includes('android')
      ? this.configService.get<string>(EnvVariableNameEnum.ANDROID_APP_STORE_URL, 'https://play.google.com/store/apps')
      : this.configService.get<string>(EnvVariableNameEnum.IOS_APP_STORE_URL, 'https://www.apple.com/app-store');
    return response.redirect(redirectUrl);
  }
}
