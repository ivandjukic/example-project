import { ConfigService } from '@nestjs/config';
import { InjectRepository } from '@nestjs/typeorm';
import { InjectTwilio, TwilioClient } from 'nestjs-twilio';
import { Injectable, UnprocessableEntityException } from '@nestjs/common';

import { EnvVariableNameEnum } from '../common/enums/env-variable-name.enum';
import { InviteEntity } from '../entity/invite.entity';
import { InviteRepository } from './invite.repository';
import { InviteTypesEnum } from './enums/invite-types.enum';
import { IsUserInvitedResponseDto } from './dto/is-user-invited-response.dto';
import { SettingKey } from '../settings/enum/settings-key.enum';
import { SettingsService } from '../settings/settings.service';
import { UserEntity } from '../entity/user.entity';
import { WaitlistService } from '../waitlist/waitlist.service';
import { isPhoneNumberValid } from '../common/phoneNumberValidator';

@Injectable()
export class InviteService {
  constructor(
    @InjectRepository(InviteEntity)
    private readonly inviteRepository: InviteRepository,
    private readonly settingsService: SettingsService,
    private readonly waitlistService: WaitlistService,
    @InjectTwilio()
    private readonly twilioClient: TwilioClient,
    private readonly configService: ConfigService
  ) {}

  async getSentInvitesByUserId(userId: string, type?: InviteTypesEnum): Promise<InviteEntity[]> {
    const query = this.inviteRepository
      .createQueryBuilder('invites')
      .addSelect(`CASE WHEN u.id is not null THEN true ELSE false END`, 'is_trusted')
      .leftJoinAndSelect('invites.invited_by_user', 'invited_by_user')
      .leftJoinAndSelect('users', 'u', 'invites.phone_number = u.phone_number')
      .where('invites.invited_by = :uid', { uid: userId });

    if (type === InviteTypesEnum.PENDING) {
      query.andWhere('u.id IS NULL');
    } else if (type === InviteTypesEnum.ACCEPTED) {
      query.andWhere('u.id IS NOT NULL');
    }
    const invites = await query.getMany();
    return invites;
  }

  async getByPhoneNumber(phoneNumber: string): Promise<InviteEntity | null> {
    return this.inviteRepository.findOne({
      where: { phone_number: phoneNumber },
    });
  }

  async getNumberOfAvailableInvitesByUserId(userId: string): Promise<number> {
    const settings = await this.settingsService.getByKey(SettingKey.MAX_INVITES_PER_USER);
    if (!settings) {
      // @TODO log this
      return 0;
    }
    const sentInvites = await this.getSentInvitesByUserId(userId);
    return Number(settings.value) - sentInvites.length;
  }

  async invite(phoneNumber: string, invitedByUser: UserEntity): Promise<void> {
    if (!isPhoneNumberValid(phoneNumber)) {
      throw new UnprocessableEntityException('invalid phone_number provided');
    }
    if ((await this.getNumberOfAvailableInvitesByUserId(invitedByUser.id)) === 0) {
      throw new UnprocessableEntityException('no invites left');
    }
    const existingInvite = await this.getByPhoneNumber(phoneNumber);
    if (existingInvite && existingInvite.invited_by === invitedByUser.id) {
      throw new UnprocessableEntityException('you have already invited this phone number');
    } else if (existingInvite) {
      throw new UnprocessableEntityException('phone number has already been invited');
    }

    await this.sendInviteSms(phoneNumber, invitedByUser);

    await this.inviteRepository.save({
      phone_number: phoneNumber,
      invited_by: invitedByUser.id,
    });
  }

  async isPhoneNumberInvited(phoneNumber): Promise<IsUserInvitedResponseDto> {
    const invite = await this.inviteRepository.findOne({
      phone_number: phoneNumber,
    });
    const waitlistRecord = await this.waitlistService.getByPhoneNumber(phoneNumber);
    return {
      is_invited: !!invite,
      is_on_waitlist: !!waitlistRecord,
    };
  }

  async sendInviteSms(phoneNumber: string, invitedByUser: UserEntity): Promise<void> {
    const url = this.configService.get<string>(EnvVariableNameEnum.INVITE_SHORT_URL, 'https//api.thetrustapp.co/invites/accept');
    const text = `${invitedByUser.first_name} ${invitedByUser.last_name} is inviting you to hear their stories on Trust, an App to ask friends about their experiences, ${url}`;
    try {
      await this.twilioClient.messages.create({
        body: text,
        from: this.configService.get<string>(EnvVariableNameEnum.TWILIO_PHONE_NUMBER, ''),
        to: phoneNumber,
      });
    } catch (e) {
      //@TODO log that user didn't receive an sms code
    }
  }
}
