import { Repository } from 'typeorm';

import { InviteEntity } from '../entity/invite.entity';

export class InviteRepository extends Repository<InviteEntity> {}
