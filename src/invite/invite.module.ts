import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { InviteController } from './invite.controller';
import { InviteEntity } from '../entity/invite.entity';
import { InviteService } from './invite.service';
import { SettingsEntity } from '../entity/settings.entity';
import { SettingsService } from '../settings/settings.service';
import { WaitlistEntity } from '../entity/waitlist.entity';
import { WaitlistService } from '../waitlist/waitlist.service';

@Module({
  imports: [TypeOrmModule.forFeature([InviteEntity, SettingsEntity, WaitlistEntity])],
  providers: [InviteService, SettingsService, WaitlistService],
  controllers: [InviteController],
})
export class InviteModule {}
