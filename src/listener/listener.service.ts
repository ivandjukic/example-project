import { InjectRepository } from '@nestjs/typeorm';
import { Injectable, NotFoundException } from '@nestjs/common';

import { ListOfListenersResponseDto } from './dto/list-of-listeners-response.dto';
import { ListenerEntity } from '../entity/listener.entity';
import { ListenerRepository } from './listener.repository';
import { ListenersRelationsEnum } from '../common/enums/listeners-relations.enum';
import { PaginationQueryRequestDto } from '../common/dto/pagination-query-request.dto';
import { UserService } from '../user/user.service';
import { getPagination } from '../common/getPagination';

@Injectable()
export class ListenerService {
  constructor(
    @InjectRepository(ListenerEntity)
    private readonly listenerRepository: ListenerRepository,
    private readonly userService: UserService
  ) {}

  async save(creatorId: string, userId: string): Promise<void> {
    const creator = await this.userService.findById(creatorId);
    if (!creator || !creator.is_creator) throw new NotFoundException();
    await this.listenerRepository.save({
      creator_id: creatorId,
      user_id: userId,
    });
  }

  async getListeners(userId: string, pagination: PaginationQueryRequestDto): Promise<ListOfListenersResponseDto> {
    const page = Number(pagination?.page ?? 1);
    const perPage = Number(pagination?.per_page ?? 20);
    const [listeners, count] = await this.listenerRepository.findAndCount({
      where: { creator_id: userId },
      relations: [ListenersRelationsEnum.USER],
      take: perPage,
      skip: (page - 1) * perPage,
      order: { created_at: 'DESC' },
    });

    return {
      listeners: listeners,
      pagination: getPagination(page, perPage, count),
    };
  }
}
