import { Repository } from 'typeorm';

import { ListenerEntity } from '../entity/listener.entity';

export class ListenerRepository extends Repository<ListenerEntity> {}
