import { ListenerDto } from './listener.dto';
import { PaginationDto } from '../../common/dto/pagination.dto';

export class ListOfListenersResponseDto {
  listeners: ListenerDto[];
  pagination: PaginationDto;
}
