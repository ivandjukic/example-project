import { ApiProperty } from '@nestjs/swagger';

export class ListenUserRequestDto {
  @ApiProperty({
    type: 'string',
    name: 'user_id',
    example: 'c08336a7-6174-484c-9e55-f96aeb10493d',
  })
  user_id: string;
}
