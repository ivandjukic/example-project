import { Module } from '@nestjs/common';

import { InviteEntity } from '../entity/invite.entity';
import { InviteService } from '../invite/invite.service';
import { ListenerController } from './listener.controller';
import { ListenerEntity } from '../entity/listener.entity';
import { ListenerService } from './listener.service';
import { OtpCodeEntity } from '../entity/otpCode.entity';
import { OtpCodeService } from '../otp-code/otp-code.service';
import { SettingsEntity } from '../entity/settings.entity';
import { SettingsService } from '../settings/settings.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserEntity } from '../entity/user.entity';
import { UserService } from '../user/user.service';
import { WaitlistEntity } from '../entity/waitlist.entity';
import { WaitlistService } from '../waitlist/waitlist.service';

@Module({
  controllers: [ListenerController],
  imports: [TypeOrmModule.forFeature([ListenerEntity, UserEntity, SettingsEntity, InviteEntity, OtpCodeEntity, WaitlistEntity])],
  providers: [ListenerService, UserService, SettingsService, InviteService, OtpCodeService, WaitlistService],
})
export class ListenerModule {}
