import { ApiBearerAuth, ApiBody, ApiOperation, ApiQuery, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Body, Controller, Get, HttpCode, HttpStatus, Post, Query, Req } from '@nestjs/common';

import { EmptyResponseDto } from '../common/dto/EmptyResponse.dto';
import { ListOfListenersResponseDto } from './dto/list-of-listeners-response.dto';
import { ListenUserRequestDto } from './dto/listen-user-request.dto';
import { ListenerService } from './listener.service';
import { PaginationQueryRequestDto } from '../common/dto/pagination-query-request.dto';
import { RequestWithUser } from '../common/interfaces/request-with-user.interface';
import { SwaggerTags } from '../common/enums/swagger-tags.enum';

@Controller('listeners')
@ApiTags(SwaggerTags.LISTENER)
export class ListenerController {
  constructor(private readonly listenerService: ListenerService) {}

  @ApiOperation({ summary: 'Listen a user' })
  @ApiBody({ type: ListenUserRequestDto })
  @ApiBearerAuth()
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Successfully saved',
    type: EmptyResponseDto,
  })
  @Post('/')
  @HttpCode(HttpStatus.OK)
  async listen(@Req() request: RequestWithUser, @Body() payload: ListenUserRequestDto): Promise<EmptyResponseDto> {
    await this.listenerService.save(payload.user_id, request.user.id);
    return {};
  }

  @ApiOperation({ summary: 'Get listeners' })
  @ApiBearerAuth()
  @ApiQuery({ type: PaginationQueryRequestDto })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Successfully fetched list of listeners',
    type: ListOfListenersResponseDto,
    isArray: true,
  })
  @Get('/')
  @HttpCode(HttpStatus.OK)
  getListeners(@Req() request: RequestWithUser, @Query() query: PaginationQueryRequestDto): Promise<ListOfListenersResponseDto> {
    return this.listenerService.getListeners(request.user.id, query);
  }
}
