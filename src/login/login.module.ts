import { LoginService } from './login.service';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { LoginEntity } from '../entity/login.entity';

@Module({
  imports: [TypeOrmModule.forFeature([LoginEntity])],
  providers: [LoginService],
})
export class LoginModule {}
