import { Repository } from 'typeorm';

import { LoginEntity } from '../entity/login.entity';

export class LoginRepository extends Repository<LoginEntity> {}
