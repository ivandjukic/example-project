import { InjectRepository } from '@nestjs/typeorm';
import { Injectable } from '@nestjs/common';

import { LoginEntity } from '../entity/login.entity';
import { LoginRepository } from './login.repository';
import { LoginTypeEnum } from './enums/login-type.enum';

@Injectable()
export class LoginService {
  constructor(
    @InjectRepository(LoginEntity)
    private readonly loginRepository: LoginRepository
  ) {}

  async save(userId: string, type: LoginTypeEnum): Promise<LoginEntity> {
    return this.loginRepository.save({
      user_id: userId,
      type,
    });
  }
}
