export enum LoginTypeEnum {
  VERIFY_PHONE_NUMBER = 'verify_phone_number',
  LOGIN = 'login',
  REFRESH = 'refresh',
}
