import { Repository } from 'typeorm';

import { WaitlistEntity } from '../entity/waitlist.entity';

export class WaitlistRepository extends Repository<WaitlistEntity> {}
