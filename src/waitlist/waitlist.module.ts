import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { WaitlistController } from './waitlist.controller';
import { WaitlistEntity } from '../entity/waitlist.entity';
import { WaitlistService } from './waitlist.service';

@Module({
  imports: [TypeOrmModule.forFeature([WaitlistEntity])],
  providers: [WaitlistService],
  controllers: [WaitlistController],
})
export class WaitlistModule {}
