import { InjectRepository } from '@nestjs/typeorm';
import { Injectable, UnprocessableEntityException } from '@nestjs/common';

import { WaitlistEntity } from '../entity/waitlist.entity';
import { WaitlistRepository } from './waitlist.repository';
import { isPhoneNumberValid } from '../common/phoneNumberValidator';

@Injectable()
export class WaitlistService {
  constructor(
    @InjectRepository(WaitlistEntity)
    private readonly waitListRepository: WaitlistRepository
  ) {}

  async getByPhoneNumber(phoneNumber: string): Promise<WaitlistEntity | null> {
    return this.waitListRepository.findOne({
      phone_number: phoneNumber,
    });
  }

  async save(phoneNumber: string): Promise<void> {
    if (!isPhoneNumberValid(phoneNumber)) {
      throw new UnprocessableEntityException('invalid phone_number provided');
    }
    if (await this.getByPhoneNumber(phoneNumber)) {
      throw new UnprocessableEntityException('phone_number already exists');
    }
    await this.waitListRepository.save({ phone_number: phoneNumber });
  }
}
