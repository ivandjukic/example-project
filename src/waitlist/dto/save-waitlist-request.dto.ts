import { ApiProperty } from '@nestjs/swagger';
import { IsString, MaxLength } from 'class-validator';

export class SaveWaitlistRequestDto {
  @ApiProperty({
    name: 'phone_number',
    type: 'string',
    example: '+16465106465',
    maxLength: 32,
  })
  @IsString()
  @MaxLength(32)
  phone_number: string;
}
