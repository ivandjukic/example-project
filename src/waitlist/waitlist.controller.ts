import { ApiBearerAuth, ApiBody, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Body, Controller, HttpCode, HttpStatus, Post } from '@nestjs/common';

import { EmptyResponseDto } from '../common/dto/EmptyResponse.dto';
import { SaveWaitlistRequestDto } from './dto/save-waitlist-request.dto';
import { SwaggerTags } from '../common/enums/swagger-tags.enum';
import { WaitlistService } from './waitlist.service';

@Controller('waitlist')
@ApiTags(SwaggerTags.WAITLIST)
export class WaitlistController {
  constructor(private readonly waitlistService: WaitlistService) {}

  @ApiOperation({ summary: 'Add phone number to the waitlist' })
  @ApiBody({ type: SaveWaitlistRequestDto })
  @ApiBearerAuth()
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Successfully returned result',
    type: EmptyResponseDto,
  })
  @Post('/')
  @HttpCode(HttpStatus.OK)
  async addToWaitlist(@Body() payload: SaveWaitlistRequestDto) {
    await this.waitlistService.save(payload.phone_number);
  }
}
