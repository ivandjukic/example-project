import { Repository } from 'typeorm';

import { FeedbackEntity } from '../entity/feedback.entity';

export class FeedbackRepository extends Repository<FeedbackEntity> {}
