import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { CircleEntity } from '../entity/circle.entity';
import { CircleMemberEntity } from '../entity/circleMember.entity';
import { CircleService } from '../circle/circle.service';
import { FeedItemEntity } from '../entity/feed-item.entity';
import { FeedItemSeenEntity } from '../entity/feed-item-seen.entity';
import { FeedItemSeenService } from '../feed-item/feed-item-seen.service';
import { FeedItemService } from '../feed-item/feed-item.service';
import { FeedItemShareEntity } from '../entity/feed-item-share.entity';
import { FeedbackController } from './feedback.controller';
import { FeedbackEntity } from '../entity/feedback.entity';
import { FeedbackService } from './feedback.service';
import { InviteEntity } from '../entity/invite.entity';
import { InviteService } from '../invite/invite.service';
import { NotificationService } from '../notification/notification.service';
import { OtpCodeEntity } from '../entity/otpCode.entity';
import { OtpCodeService } from '../otp-code/otp-code.service';
import { SettingsEntity } from '../entity/settings.entity';
import { SettingsService } from '../settings/settings.service';
import { UserEntity } from '../entity/user.entity';
import { UserService } from '../user/user.service';
import { WaitlistEntity } from '../entity/waitlist.entity';
import { WaitlistService } from '../waitlist/waitlist.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      FeedbackEntity,
      FeedItemEntity,
      FeedItemShareEntity,
      CircleEntity,
      CircleMemberEntity,
      UserEntity,
      FeedItemSeenEntity,
      SettingsEntity,
      InviteEntity,
      OtpCodeEntity,
      WaitlistEntity,
    ]),
  ],
  providers: [
    FeedbackService,
    FeedItemService,
    CircleService,
    NotificationService,
    UserService,
    FeedItemSeenService,
    SettingsService,
    InviteService,
    OtpCodeService,
    WaitlistService,
  ],
  controllers: [FeedbackController],
})
export class FeedbackModule {}
