import { InjectRepository } from '@nestjs/typeorm';
import { Injectable } from '@nestjs/common';

import { FeedItemService } from '../feed-item/feed-item.service';
import { FeedbackEntity } from '../entity/feedback.entity';
import { FeedbackRepository } from './feedback.repository';
import { SaveFeedbackRequestDto } from './dto/save-feedback-request.dto';

@Injectable()
export class FeedbackService {
  constructor(
    @InjectRepository(FeedbackEntity)
    private readonly feedbackRepository: FeedbackRepository,
    private readonly feedItemService: FeedItemService
  ) {}

  async save(payload: SaveFeedbackRequestDto, userId: string): Promise<void> {
    await this.feedbackRepository.save({
      user_id: userId,
      rate: payload.rate,
      feedback: payload.feedback,
      os_type: payload.os_type,
      os_version: payload.os_version,
      app_version: payload.app_version,
      device: payload.device,
    });
  }

  async getByUserId(userId: string): Promise<FeedbackEntity[]> {
    return this.feedbackRepository.find({
      user_id: userId,
    });
  }

  async shouldDisplayModal(userId: string): Promise<boolean> {
    const REQUIRED_NUMBER_OF_STORIES_FOR_FEEDBACK_MODAL = 3;
    const feedbacks = await this.getByUserId(userId);
    if (feedbacks.length) {
      return false;
    }
    const numberOfCreatedQuestions = await this.feedItemService.getNumberOfCreatedQuestionsByUserId(userId);
    return numberOfCreatedQuestions >= REQUIRED_NUMBER_OF_STORIES_FOR_FEEDBACK_MODAL;
  }
}
