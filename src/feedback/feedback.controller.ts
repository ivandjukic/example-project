import { ApiBody, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Body, Controller, Get, HttpCode, HttpStatus, Post, Req } from '@nestjs/common';

import { EmptyResponseDto } from '../common/dto/EmptyResponse.dto';
import { FeedbackService } from './feedback.service';
import { RequestWithUser } from '../common/interfaces/request-with-user.interface';
import { SaveFeedbackRequestDto } from './dto/save-feedback-request.dto';
import { ShouldDisplayFeedbackModalResponseDto } from './dto/should-display-feedback-modal-response.dto';
import { SwaggerTags } from '../common/enums/swagger-tags.enum';

@Controller('feedbacks')
@ApiTags(SwaggerTags.FEEDBACK)
export class FeedbackController {
  constructor(private readonly feedbackService: FeedbackService) {}

  @ApiOperation({ summary: 'Save feedback' })
  @ApiBody({ type: SaveFeedbackRequestDto })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Successfully sent sms OTP code',
    type: EmptyResponseDto,
  })
  @ApiResponse({
    status: HttpStatus.FORBIDDEN,
    description: "Hm, that didn't work. Please try again.",
  })
  @Post('/')
  @HttpCode(HttpStatus.OK)
  async save(@Req() request: RequestWithUser, @Body() payload: SaveFeedbackRequestDto) {
    return this.feedbackService.save(payload, request.user.id);
  }

  @ApiOperation({ summary: 'Should display feedback modal' })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Successfully returned result',
    type: ShouldDisplayFeedbackModalResponseDto,
  })
  @Get('/should-display-modal')
  @HttpCode(HttpStatus.OK)
  async shouldDisplayModal(@Req() request: RequestWithUser) {
    return {
      should_display: await this.feedbackService.shouldDisplayModal(request.user.id),
    };
  }
}
