import { ApiProperty } from '@nestjs/swagger';
import { IsNumber, IsOptional, IsString } from 'class-validator';

export class SaveFeedbackRequestDto {
  @ApiProperty({
    name: 'rate',
    type: 'number',
    example: 1,
  })
  @IsNumber()
  rate: string;

  @ApiProperty({
    name: 'feedback',
    type: 'string',
    example: 'This is an amazing app!',
  })
  @IsString()
  feedback: string;

  @ApiProperty({
    name: 'os_type',
    type: 'string',
    example: 'IO!',
    required: false,
    maxLength: 100,
  })
  @IsString()
  @IsOptional()
  os_type: string;

  @ApiProperty({
    name: 'os_version',
    type: 'string',
    example: '10.1',
    required: false,
    maxLength: 100,
  })
  @IsString()
  @IsOptional()
  os_version: string;

  @ApiProperty({
    name: 'device',
    type: 'string',
    example: 'Iphone 10',
    required: false,
    maxLength: 100,
  })
  @IsString()
  @IsOptional()
  device: string;

  @ApiProperty({
    name: 'app_version',
    type: 'string',
    example: '0.3.5',
    required: false,
    maxLength: 100,
  })
  @IsString()
  @IsOptional()
  app_version: string;
}
