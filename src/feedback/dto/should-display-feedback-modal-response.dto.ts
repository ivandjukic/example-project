import { ApiProperty } from '@nestjs/swagger';

export class ShouldDisplayFeedbackModalResponseDto {
  @ApiProperty({
    type: 'boolean',
    name: 'should_display',
    example: true,
  })
  should_display: boolean;
}
