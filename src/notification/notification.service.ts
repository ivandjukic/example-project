import { Inject, LoggerService } from '@nestjs/common';
import { WINSTON_MODULE_NEST_PROVIDER } from 'nest-winston';
import axios from 'axios';

import { FeedItemEntity } from '../entity/feed-item.entity';
import { Notification, NotificationPriority, NotificationTypes } from './interfaces/notification.interface';
import { UserEntity } from '../entity/user.entity';

export class NotificationService {
  private readonly EXPO_NOTIFICATION_URL = 'https://exp.host/--/api/v2/push/send';

  constructor(
    @Inject(WINSTON_MODULE_NEST_PROVIDER)
    private readonly logService: LoggerService
  ) {}

  async send(notification: Notification, to: string[]): Promise<void> {
    this.logService.log({ message: 'SEND NOTIFICATION: STARTED', body: { notification, to } });
    if (!to.length) {
      this.logService.log({ message: 'empty to[] provided' });
      return;
    }
    const response = await axios.post(this.EXPO_NOTIFICATION_URL, {
      to,
      title: notification.title,
      body: notification.body,
      priority: notification.priority ?? NotificationPriority.HIGH,
      data: notification.data ?? {},
    });
    this.logService.log({ message: 'SEND NOTIFICATION: FINISHED', body: { response: response.data } });
  }

  async userTrustedNotification(fromUser: UserEntity, toUser: UserEntity): Promise<void> {
    const notification: Notification = {
      title: 'Trust',
      body: `${fromUser.first_name} ${fromUser.last_name} trusted you.`,
      priority: NotificationPriority.HIGH,
      data: {
        type: NotificationTypes.USER_TRUSTED_BY_OTHER_USER,
      },
    };
    await this.send(notification, [toUser.device_token.token]);
  }

  async questionReceivedNotification(fromUser: UserEntity, toUsers: UserEntity[], feedItem: FeedItemEntity): Promise<void> {
    const notification: Notification = {
      title: 'Trust',
      body: `${fromUser.first_name} ${fromUser.last_name} asked you a question.`,
      priority: NotificationPriority.HIGH,
      data: {
        feed_item: feedItem,
      },
    };
    const toUserIds = toUsers.filter((user) => user && user.device_token).map((user) => user.device_token.token);

    await this.send(notification, toUserIds);
  }

  async questionRespondedNotification(fromUser: UserEntity, toUser: UserEntity, feedItem: FeedItemEntity): Promise<void> {
    if (fromUser.id === toUser.id) return;
    if (toUser?.device_token?.token) {
      const notification: Notification = {
        title: 'Trust',
        body: `${fromUser.first_name} ${fromUser.last_name} responded to your question.`,
        priority: NotificationPriority.HIGH,
        data: {
          feed_item: feedItem,
        },
      };
      await this.send(notification, [toUser.device_token.token]);
    }
  }
  async storyRespondedNotification(fromUser: UserEntity, toUser: UserEntity, feedItem: FeedItemEntity): Promise<void> {
    if (fromUser.id === toUser.id) return;
    const notification: Notification = {
      title: 'Trust',
      body: `${fromUser.first_name} ${fromUser.last_name} responded to your story.`,
      priority: NotificationPriority.HIGH,
      data: {
        feed_item: feedItem,
      },
    };
    if (toUser?.device_token?.token) {
      await this.send(notification, [toUser.device_token.token]);
    }
  }

  async storyToldNotification(fromUser: UserEntity, toUser: UserEntity, feedItem: FeedItemEntity): Promise<void> {
    if (fromUser.id === toUser.id) return;
    const notification: Notification = {
      title: 'Trust',
      body: `${fromUser.first_name} ${fromUser.last_name} told you a story.`,
      priority: NotificationPriority.HIGH,
      data: {
        feed_item: feedItem,
      },
    };
    if (toUser?.device_token?.token) {
      await this.send(notification, [toUser.device_token.token]);
    }
  }

  async storyShared(fromUser: UserEntity, toUsers: UserEntity[], feedItem: FeedItemEntity): Promise<void> {
    const notification: Notification = {
      title: 'Trust',
      body: `${fromUser.first_name} ${fromUser.last_name} told a new story.`,
      priority: NotificationPriority.HIGH,
      data: {
        feed_item: feedItem,
      },
    };
    const toUserIds = toUsers.filter((user) => user && user.device_token).map((user) => user.device_token.token);

    await this.send(notification, toUserIds);
  }
}
