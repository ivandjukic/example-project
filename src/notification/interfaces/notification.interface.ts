import { FeedItemEntity } from '../../entity/feed-item.entity';

export enum NotificationPriority {
  MIN = 'min',
  LOW = 'low',
  DEFAULT = 'default',
  HIGH = 'high',
  MAX = 'max',
}

export interface NotificationData {
  feed_item?: FeedItemEntity;
  type?: NotificationTypes;
}

export enum NotificationTypes {
  USER_TRUSTED_BY_OTHER_USER = 'USER_TRUSTED_BY_OTHER_USER',
}

export interface Notification {
  title: string;
  body: string;
  priority?: NotificationPriority;
  data?: NotificationData;
}
