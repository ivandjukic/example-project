import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { ProfilePhotoController } from './profile-photo.controller';
import { ProfilePhotoEntity } from '../entity/profile-photo.entity';
import { ProfilePhotoService } from './profile-photo.service';

@Module({
  imports: [TypeOrmModule.forFeature([ProfilePhotoEntity])],
  controllers: [ProfilePhotoController],
  providers: [ProfilePhotoService],
})
export class ProfilePhotoModule {}
