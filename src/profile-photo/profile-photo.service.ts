import { Express } from 'express';
import { InjectRepository } from '@nestjs/typeorm';
import { Injectable, UnprocessableEntityException } from '@nestjs/common';
import { S3 } from 'aws-sdk';
import { v4 } from 'uuid';

import { ConfigService } from '@nestjs/config';
import { EnvVariableNameEnum } from '../common/enums/env-variable-name.enum';
import { ProfilePhotoEntity } from '../entity/profile-photo.entity';
import { ProfilePhotoRepository } from './profile-photo.repository';

@Injectable()
export class ProfilePhotoService {
  constructor(
    @InjectRepository(ProfilePhotoEntity)
    private readonly profilePhotoRepository: ProfilePhotoRepository,
    private readonly configService: ConfigService
  ) {}

  async findByUrl(url: string): Promise<ProfilePhotoEntity | null> {
    return this.profilePhotoRepository.findOne({
      url,
    });
  }

  async upload(payload: Express.Multer.File, userId?: string): Promise<ProfilePhotoEntity> {
    const { mimetype, size, buffer } = payload;
    const MAX_UPLOAD_SIZE = 20 * 1024 * 1024; // 20 MB
    const SUPPORTED_FORMATS = ['image/png', 'image/jpg', 'image/jpeg', 'image/gif'];
    const extension = mimetype.split('/')[1] ?? '';

    if (size > MAX_UPLOAD_SIZE) {
      throw new UnprocessableEntityException({
        message: 'File too large',
        max_upload_size: `${MAX_UPLOAD_SIZE / (1024 * 1024)} MB`,
      });
    }
    if (!SUPPORTED_FORMATS.includes(mimetype)) {
      throw new UnprocessableEntityException({
        message: 'Unsupported file format',
        supported_formats: SUPPORTED_FORMATS,
      });
    }
    const s3Client = new S3({
      accessKeyId: this.configService.get<string>(EnvVariableNameEnum.AWS_ACCESS_KEY_ID, ''),
      secretAccessKey: this.configService.get<string>(EnvVariableNameEnum.AWS_SECRET_ACCESS_KEY, ''),
      region: this.configService.get<string>(EnvVariableNameEnum.AWS_REGION, ''),
    });

    const response = await s3Client
      .upload({
        Bucket: this.configService.get<string>(EnvVariableNameEnum.AWS_BUCKET_NAME, ''),
        Key: `test/${v4()}.${extension}`,
        ContentType: mimetype,
        Body: buffer,
        ACL: 'public-read',
      })
      .promise();

    await this.profilePhotoRepository.upsert(
      {
        user_id: userId,
        url: response.Location,
      },
      ['user_id']
    );

    const profilePhoto = await this.findByUrl(response.Location);

    return profilePhoto;
  }

  async delete(userId: string): Promise<void> {
    await this.profilePhotoRepository.delete({ user_id: userId });
  }
}
