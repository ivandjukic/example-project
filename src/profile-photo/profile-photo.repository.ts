import { Repository } from 'typeorm';

import { ProfilePhotoEntity } from '../entity/profile-photo.entity';

export class ProfilePhotoRepository extends Repository<ProfilePhotoEntity> {}
