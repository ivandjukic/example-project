import { ApiBearerAuth, ApiBody, ApiConsumes, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Controller, Delete, HttpCode, HttpStatus, Post, Req, UploadedFile, UseInterceptors } from '@nestjs/common';
import { Express } from 'express';
import { FileInterceptor } from '@nestjs/platform-express';

import { ProfilePhotoDto } from './dto/profile-photo.dto';
import { ProfilePhotoService } from './profile-photo.service';
import { RequestWithUser } from '../common/interfaces/request-with-user.interface';
import { SwaggerTags } from '../common/enums/swagger-tags.enum';
import { UploadRequestDto } from '../upload/dto/upload-request.dto';

@Controller('profile-photos')
@ApiTags(SwaggerTags.PROFILE_PHOTO)
export class ProfilePhotoController {
  constructor(private readonly profilePhotoService: ProfilePhotoService) {}

  @Post()
  @ApiOperation({ summary: 'Upload profile photo' })
  @ApiBearerAuth()
  @ApiConsumes('multipart/form-data')
  @ApiBody({ type: UploadRequestDto })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Successfully uploaded file',
    type: ProfilePhotoDto,
  })
  @HttpCode(HttpStatus.OK)
  @UseInterceptors(FileInterceptor('file'))
  async upload(@Req() request: RequestWithUser, @UploadedFile() payload: Express.Multer.File): Promise<ProfilePhotoDto> {
    return this.profilePhotoService.upload(payload, request.user.id);
  }

  @Delete()
  @ApiOperation({ summary: 'Delete profile photo' })
  @ApiBearerAuth()
  @HttpCode(HttpStatus.OK)
  @UseInterceptors(FileInterceptor('file'))
  async delete(@Req() request: RequestWithUser) {
    return this.profilePhotoService.delete(request.user.id);
  }
}
