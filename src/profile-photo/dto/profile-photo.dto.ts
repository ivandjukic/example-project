import { ApiProperty } from '@nestjs/swagger';

export class ProfilePhotoDto {
  @ApiProperty({
    type: 'uuid',
    name: 'id',
    example: '1058dcb4-6947-4a80-9ed9-53d3d70c4a6b',
    required: true,
  })
  id: string;

  @ApiProperty({
    type: 'string',
    name: 'url',
    example: 'https://thetrustapp.s3.us-east-2.amazonaws.com/test/0c1fab59-86ab-4973-ab8d-39ccef987953.m4a',
    required: true,
  })
  url: string;

  @ApiProperty({
    type: 'uuid',
    name: 'user_id',
    example: '1058dcb4-6947-4a80-9ed9-53d3d70c4a55',
    required: true,
  })
  user_id: string;

  @ApiProperty({
    type: 'date',
    name: 'created_at',
    example: '2020-01-01 15:15:15',
    required: true,
  })
  created_at: Date;

  @ApiProperty({
    type: 'date',
    name: 'updated_at',
    example: '2020-01-01 15:15:15',
    required: true,
  })
  updated_at: Date;
}
