import { ApiProperty } from '@nestjs/swagger';
import { Express } from 'express';

export class UploadRequestDto {
  @ApiProperty({
    name: 'file',
    type: 'file',
    format: 'binary',
    required: true,
    example: 'Audio file',
  })
  file: Express.Multer.File;
}
