import { ApiProperty } from '@nestjs/swagger';
import { FeedItemTypeEnum } from '../enum/feed-item-type.enum';
import { FeedItemVisibilityEnum } from '../enum/feed-item-visibility.enum';
import { IsArray, IsEnum, IsOptional, IsString } from 'class-validator';

export class SaveFeedItemRequestDto {
  @ApiProperty({
    name: 'title',
    type: 'string',
    required: true,
    example: 'some dummy title',
  })
  @IsString()
  title: string;

  @ApiProperty({
    name: 'media_id',
    type: 'string',
    required: true,
    example: '8608711d-7aa1-4148-a1f7-c1d2e85df0af',
  })
  @IsString()
  media_id: string;

  @ApiProperty({
    name: 'type',
    type: 'enum',
    example: FeedItemTypeEnum.STORY,
    required: true,
    maxLength: 100,
  })
  @IsEnum(FeedItemTypeEnum)
  type: FeedItemTypeEnum;

  @ApiProperty({
    name: 'visibility',
    type: 'string',
    example: FeedItemVisibilityEnum.LIMITED,
    required: true,
    maxLength: 100,
  })
  @IsEnum(FeedItemVisibilityEnum)
  visibility: FeedItemVisibilityEnum;

  @ApiProperty({
    name: 'shared_with',
    type: 'array',
    example: ['8608711d-7aa1-4148-a1f7-c1d2e85df0af'],
    isArray: true,
  })
  @IsArray()
  @IsOptional()
  shared_with?: string[];

  @ApiProperty({
    name: 'responding_to_feed_item_id',
    type: 'string',
    example: '8608711d-7aa1-4148-a1f7-c1d2e85df0af',
  })
  @IsString()
  @IsOptional()
  responding_to_feed_item_id?: string;
}
