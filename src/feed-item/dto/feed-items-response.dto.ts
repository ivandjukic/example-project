import { FeedItemDto } from './feed-item.dto';
import { PaginationDto } from '../../common/dto/pagination.dto';

export class FeedItemsResponseDto {
  feed_items: FeedItemDto[];
  pagination: PaginationDto;
}
