import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

import { FeedItemSeenDto } from './feed-item-seen.dto';
import { FeedItemShareWithEntityDto } from './feed-item-share-with-entity.dto';
import { FeedItemTypeEnum } from '../enum/feed-item-type.enum';
import { FeedItemVisibilityEnum } from '../enum/feed-item-visibility.enum';
import { MediaDto } from '../../upload/dto/media.dto';
import { UserDto } from '../../user/dto/user.dto';

export class FeedItemDto {
  @ApiProperty({
    type: 'string',
    name: 'id',
    example: '0dc4f28e-44c4-4202-afa6-b63e24623f9e',
    required: true,
  })
  id: string;

  @ApiProperty({
    type: 'string',
    name: 'type',
    example: FeedItemTypeEnum.STORY,
    maxLength: 10,
    required: true,
    enum: FeedItemTypeEnum,
  })
  type: FeedItemTypeEnum;

  @ApiProperty({
    type: 'string',
    name: 'visibility',
    example: FeedItemVisibilityEnum.LIMITED,
    maxLength: 10,
    required: true,
    enum: FeedItemVisibilityEnum,
  })
  visibility: FeedItemVisibilityEnum;

  @ApiProperty({
    type: 'string',
    name: 'author_id',
    example: '0dc4f28e-44c4-4202-afa6-b63e24623f55',
    required: true,
  })
  author_id: string;

  author: UserDto;

  @ApiProperty({
    type: 'string',
    name: 'media_id',
    example: '0dc4f28e-44c4-4202-afa6-b63e24623f55',
    required: true,
  })
  media_id: string;

  media: MediaDto;

  @ApiProperty({
    type: 'string',
    name: 'responding_to_feed_item_id',
    example: '0dc4f28e-44c4-4202-afa6-b63e24623f55',
    required: false,
  })
  responding_to_feed_item_id?: string;

  @ApiProperty({
    type: FeedItemShareWithEntityDto,
    name: 'shared_with',
    required: false,
    isArray: true,
  })
  shared_with?: FeedItemShareWithEntityDto[];

  @ApiProperty({
    type: 'date',
    name: 'created_at',
    example: '2020-01-01 15:15:15',
    required: true,
  })
  created_at: Date;

  @ApiProperty({
    type: 'date',
    name: 'updated_at',
    example: '2020-01-01 15:15:15',
    required: true,
  })
  updated_at: Date;

  @ApiPropertyOptional({
    name: 'deleted_at',
    example: '2020-01-01 15:15:15',
    required: false,
  })
  deleted_at?: Date;

  @ApiProperty({
    type: FeedItemDto,
    name: 'responding_feed_items',
    example: {
      id: 'ae54fe8c-08e5-49b8-b2f5-5b5630a783ec',
      author_id: '03bf652e-5b07-471d-bd1d-86d13fd5b9c4',
      title: 'blabla',
      type: 'story',
      visibility: 'public',
      media_id: 'c9cde751-ff2a-445d-9f88-9cb0d402d6b1',
      responding_to_feed_item_id: 'f9b5aa64-c157-4311-a3e9-3871124d7da7',
      created_at: '2022-03-02T15:29:04.186Z',
      updated_at: '2022-03-02T15:29:04.186Z',
      deleted_at: null,
    },
    isArray: true,
  })
  responding_feed_items: FeedItemDto[];

  @ApiProperty({
    name: 'seens',
    required: true,
    isArray: true,
  })
  seens?: FeedItemSeenDto[];
}
