import { ApiProperty } from '@nestjs/swagger';

export class FeedItemSeenDto {
  @ApiProperty({
    type: 'string',
    name: 'id',
    example: '0dc4f28e-44c4-4202-afa6-b63e24623f9e',
    required: true,
  })
  id: string;

  @ApiProperty({
    type: 'string',
    name: 'user_id',
    example: '0dc4f28e-44c4-4202-afa6-b63e24623f55',
    required: true,
  })
  user_id: string;

  @ApiProperty({
    type: 'string',
    name: 'feed_item_id',
    example: '0dc4f28e-44c4-4202-afa6-b63e24623f55',
    required: true,
  })
  feed_item_id: string;

  @ApiProperty({
    type: 'date',
    name: 'created_at',
    example: '2020-01-01 15:15:15',
    required: true,
  })
  created_at: Date;

  @ApiProperty({
    type: 'date',
    name: 'updated_at',
    example: '2020-01-01 15:15:15',
    required: true,
  })
  updated_at: Date;
}
