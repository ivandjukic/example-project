import { Repository } from 'typeorm';

import { FeedItemEntity } from '../entity/feed-item.entity';

export class FeedItemRepository extends Repository<FeedItemEntity> {}
