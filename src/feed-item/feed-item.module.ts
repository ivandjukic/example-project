import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { CircleEntity } from '../entity/circle.entity';
import { CircleMemberEntity } from '../entity/circleMember.entity';
import { CircleService } from '../circle/circle.service';
import { FeedItemController } from './feed-item.controller';
import { FeedItemEntity } from '../entity/feed-item.entity';
import { FeedItemSeenEntity } from '../entity/feed-item-seen.entity';
import { FeedItemSeenService } from './feed-item-seen.service';
import { FeedItemService } from './feed-item.service';
import { FeedItemShareEntity } from '../entity/feed-item-share.entity';
import { InviteEntity } from '../entity/invite.entity';
import { InviteService } from '../invite/invite.service';
import { NotificationService } from '../notification/notification.service';
import { OtpCodeEntity } from '../entity/otpCode.entity';
import { OtpCodeService } from '../otp-code/otp-code.service';
import { SettingsEntity } from '../entity/settings.entity';
import { SettingsService } from '../settings/settings.service';
import { UserEntity } from '../entity/user.entity';
import { UserService } from '../user/user.service';
import { WaitlistEntity } from '../entity/waitlist.entity';
import { WaitlistService } from '../waitlist/waitlist.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      FeedItemEntity,
      FeedItemShareEntity,
      CircleEntity,
      CircleMemberEntity,
      UserEntity,
      SettingsEntity,
      InviteEntity,
      OtpCodeEntity,
      FeedItemSeenEntity,
      WaitlistEntity,
    ]),
  ],
  providers: [
    FeedItemService,
    CircleService,
    UserService,
    SettingsService,
    InviteService,
    OtpCodeService,
    NotificationService,
    FeedItemSeenService,
    WaitlistService,
  ],
  controllers: [FeedItemController],
})
export class FeedItemModule {}
