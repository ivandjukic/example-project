import { Repository } from 'typeorm';

import { FeedItemShareEntity } from '../entity/feed-item-share.entity';

export class FeedItemShareRepository extends Repository<FeedItemShareEntity> {}
