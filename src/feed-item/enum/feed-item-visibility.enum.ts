export enum FeedItemVisibilityEnum {
  PUBLIC = 'public',
  PRIVATE = 'private',
  LIMITED = 'limited',
}
