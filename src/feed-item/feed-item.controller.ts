import { ApiBearerAuth, ApiBody, ApiOperation, ApiQuery, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Body, Controller, Delete, Get, HttpCode, HttpStatus, Param, Post, Query, Req } from '@nestjs/common';

import { ApiImplicitParam } from '@nestjs/swagger/dist/decorators/api-implicit-param.decorator';
import { FeedItemDto } from './dto/feed-item.dto';
import { FeedItemService } from './feed-item.service';
import { FeedItemTypeEnum } from './enum/feed-item-type.enum';
import { FeedItemsResponseDto } from './dto/feed-items-response.dto';
import { PaginationQueryRequestDto } from '../common/dto/pagination-query-request.dto';
import { RequestWithUser } from '../common/interfaces/request-with-user.interface';
import { SaveFeedItemRequestDto } from './dto/save-feed-item-request.dto';
import { SwaggerTags } from '../common/enums/swagger-tags.enum';

@Controller('feed-items')
@ApiTags(SwaggerTags.FEED_ITEM)
export class FeedItemController {
  constructor(private readonly feedItemService: FeedItemService) {}

  @Post('/')
  @ApiOperation({ summary: 'Save feed item' })
  @ApiBody({ type: SaveFeedItemRequestDto })
  @ApiBearerAuth()
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Successfully saved feed item',
    type: FeedItemDto,
  })
  @HttpCode(HttpStatus.OK)
  save(@Req() request: RequestWithUser, @Body() payload: SaveFeedItemRequestDto): Promise<FeedItemDto> {
    return this.feedItemService.save(payload, request.user);
  }

  @Get('/profile')
  @ApiOperation({ summary: 'Get profile feed' })
  @ApiBearerAuth()
  @ApiQuery({ type: PaginationQueryRequestDto })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Successfully fetched profile feed',
    type: FeedItemDto,
    isArray: true,
  })
  @HttpCode(HttpStatus.OK)
  getProfileFeed(@Req() request: RequestWithUser, @Query() query: PaginationQueryRequestDto): Promise<FeedItemsResponseDto> {
    return this.feedItemService.getProfileFeed(request.user.id, query);
  }

  @Get('/home')
  @ApiOperation({ summary: 'Get home feed' })
  @ApiBearerAuth()
  @ApiQuery({ type: PaginationQueryRequestDto })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Successfully fetched home feed',
    type: FeedItemDto,
    isArray: true,
  })
  @HttpCode(HttpStatus.OK)
  getHomeFeed(@Req() request: RequestWithUser, @Query() query: PaginationQueryRequestDto): Promise<FeedItemsResponseDto> {
    return this.feedItemService.getHomeFeed(request.user.id, query);
  }

  @Get('/home/trusted')
  @ApiOperation({ summary: 'Get home feed from trusted users only' })
  @ApiBearerAuth()
  @ApiQuery({ type: PaginationQueryRequestDto })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Successfully fetched home feed from trusted users',
    type: FeedItemDto,
    isArray: true,
  })
  @HttpCode(HttpStatus.OK)
  getHomeFeedFromTrustedUsers(@Req() request: RequestWithUser, @Query() query: PaginationQueryRequestDto): Promise<FeedItemsResponseDto> {
    return this.feedItemService.getHomeFeedFromTrustedUsersOnly(request.user.id, query);
  }

  @Get('/:id')
  @ApiOperation({ summary: 'Get feed item by id' })
  @ApiBearerAuth()
  @ApiImplicitParam({
    name: 'id',
    type: 'string',
    required: true,
    example: '8608711d-7aa1-4148-a1f7-c1d2e85df0af',
  })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Successfully fetched feed item',
    type: FeedItemDto,
  })
  @HttpCode(HttpStatus.OK)
  getById(@Param('id') id: string, @Req() request: RequestWithUser): Promise<FeedItemDto> {
    return this.feedItemService.findByIdAndCheckIfUserIsAuthorized(id, request.user.id);
  }

  @Delete('/:id')
  @ApiOperation({ summary: 'Delete feed item' })
  @ApiBearerAuth()
  @ApiImplicitParam({
    name: 'id',
    type: 'string',
    required: true,
    example: '8608711d-7aa1-4148-a1f7-c1d2e85df0af',
  })
  @HttpCode(HttpStatus.OK)
  delete(@Param('id') id: string, @Req() request: RequestWithUser) {
    return this.feedItemService.delete(id, request.user.id);
  }

  @Get('/profile/:feedItemType')
  @ApiOperation({ summary: 'Get created feed items by feed item type' })
  @ApiBearerAuth()
  @ApiImplicitParam({
    name: 'feed-item-type',
    type: 'string',
    required: true,
    example: FeedItemTypeEnum.STORY,
    enum: FeedItemTypeEnum,
  })
  @ApiQuery({ type: PaginationQueryRequestDto })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Successfully fetched feed items',
    type: FeedItemDto,
    isArray: true,
  })
  @HttpCode(HttpStatus.OK)
  getCreatedFeedItemsByFeedItemType(
    @Req() request: RequestWithUser,
    @Param('feedItemType') feedItemType: FeedItemTypeEnum,
    @Query() query: PaginationQueryRequestDto
  ): Promise<FeedItemsResponseDto> {
    return this.feedItemService.getCreatedFeedItemsByFeedItemType(request.user.id, feedItemType, query);
  }

  @Get('/profile/:feedItemType/received')
  @ApiOperation({ summary: 'Get received feed items by feed item type' })
  @ApiBearerAuth()
  @ApiImplicitParam({
    name: 'feed-item-type',
    type: 'string',
    required: true,
    example: FeedItemTypeEnum.STORY,
    enum: FeedItemTypeEnum,
  })
  @ApiQuery({ type: PaginationQueryRequestDto })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Successfully fetched feed items',
    type: FeedItemDto,
    isArray: true,
  })
  @HttpCode(HttpStatus.OK)
  getReceivedFeedItemsByFeedItemType(
    @Req() request: RequestWithUser,
    @Param('feedItemType') feedItemType: FeedItemTypeEnum,
    @Query() query: PaginationQueryRequestDto
  ): Promise<FeedItemsResponseDto> {
    return this.feedItemService.getReceivedFeedItemsByFeedItemType(request.user.id, feedItemType, query);
  }
}
