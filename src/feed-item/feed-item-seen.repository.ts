import { Repository } from 'typeorm';

import { FeedItemSeenEntity } from '../entity/feed-item-seen.entity';

export class FeedItemSeenRepository extends Repository<FeedItemSeenEntity> {}
