import { In, IsNull, Not } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Injectable, NotFoundException, UnprocessableEntityException } from '@nestjs/common';

import { CircleRelationsEnum } from '../common/enums/circle-relations.enum';
import { CircleService } from '../circle/circle.service';
import { FeedItemDto } from './dto/feed-item.dto';
import { FeedItemEntity } from '../entity/feed-item.entity';
import { FeedItemRelationsEnum } from '../common/enums/feed-item-relations.enum';
import { FeedItemRepository } from './feed-item.repository';
import { FeedItemSeenService } from './feed-item-seen.service';
import { FeedItemShareEntity } from '../entity/feed-item-share.entity';
import { FeedItemShareRepository } from './feed-item-share.repository';
import { FeedItemTypeEnum } from './enum/feed-item-type.enum';
import { FeedItemVisibilityEnum } from './enum/feed-item-visibility.enum';
import { FeedItemsResponseDto } from './dto/feed-items-response.dto';
import { NotificationService } from '../notification/notification.service';
import { PaginationQueryRequestDto } from '../common/dto/pagination-query-request.dto';
import { SaveFeedItemRequestDto } from './dto/save-feed-item-request.dto';
import { UserEntity } from '../entity/user.entity';
import { UserService } from '../user/user.service';
import { getPagination } from '../common/getPagination';

@Injectable()
export class FeedItemService {
  constructor(
    @InjectRepository(FeedItemEntity)
    private readonly feedItemRepository: FeedItemRepository,
    @InjectRepository(FeedItemShareEntity)
    private readonly feedItemShareRepository: FeedItemShareRepository,
    private readonly circleService: CircleService,
    private readonly notificationService: NotificationService,
    private readonly userService: UserService,
    private readonly feedItemSeenService: FeedItemSeenService
  ) {}

  async findByMediaId(mediaId: string): Promise<FeedItemEntity | null> {
    return this.feedItemRepository.findOne(
      { media_id: mediaId },
      {
        relations: [FeedItemRelationsEnum.AUTHOR, FeedItemRelationsEnum.MEDIA, FeedItemRelationsEnum.RESPONDING_TO_FEED_ITEM],
        order: { created_at: 'DESC' },
      }
    );
  }

  findById(
    id: string,
    relations: FeedItemRelationsEnum[] = [FeedItemRelationsEnum.AUTHOR, FeedItemRelationsEnum.MEDIA]
  ): Promise<FeedItemEntity | null> {
    return this.feedItemRepository.findOne({ id }, { relations });
  }

  async save(payload: SaveFeedItemRequestDto, user: UserEntity): Promise<FeedItemDto> {
    if (payload.visibility === FeedItemVisibilityEnum.LIMITED && (!Array.isArray(payload.shared_with) || !payload.shared_with.length)) {
      throw new UnprocessableEntityException('shared_with is required');
    }

    await this.feedItemRepository.save({
      author_id: user.id,
      visibility: payload.visibility,
      type: payload.type,
      title: payload.title,
      media_id: payload.media_id,
      responding_to_feed_item_id: payload.responding_to_feed_item_id,
    });

    const feedItem = await this.findByMediaId(payload.media_id);

    // if feed item responding to another one, update `updated_at` from the original one in order to push original one
    if (payload.responding_to_feed_item_id) {
      const respondingOnFeedItemId = await this.feedItemRepository.findOne({ id: payload.responding_to_feed_item_id });
      await this.feedItemRepository.update({ id: payload.responding_to_feed_item_id }, { updated_at: new Date() });
      // if the responding story is nested updated the updated_at of original story
      if (respondingOnFeedItemId.responding_to_feed_item_id) {
        await this.feedItemRepository.update({ id: respondingOnFeedItemId.responding_to_feed_item_id }, { updated_at: new Date() });
      }
    }

    // save feed item shares
    if (Array.isArray(payload.shared_with) && payload.shared_with.length) {
      this.feedItemShareRepository.save(
        payload.shared_with.map((userId: string) => {
          return {
            feed_item_id: feedItem.id,
            user_id: userId,
          };
        })
      );
    }
    // handle this using events once it's ready
    await this.sendPushNotifications(payload, user, feedItem);
    return feedItem;
  }

  async sendPushNotifications(payload: SaveFeedItemRequestDto, user: UserEntity, feedItem: FeedItemEntity): Promise<void> {
    if (
      payload.type === FeedItemTypeEnum.QUESTION &&
      payload.visibility === FeedItemVisibilityEnum.LIMITED &&
      Array.isArray(payload.shared_with) &&
      payload.shared_with.length
    ) {
      await this.notificationService.questionReceivedNotification(
        user,
        await Promise.all(payload.shared_with.map((userId: string) => this.userService.findById(userId))),
        feedItem
      );
    } else if (payload.type === FeedItemTypeEnum.QUESTION && payload.visibility === FeedItemVisibilityEnum.PUBLIC) {
      const circle = await this.circleService.getByUserId(user.id, [CircleRelationsEnum.MEMBERS, CircleRelationsEnum.MEMBER_USER]);
      await this.notificationService.questionReceivedNotification(
        user,
        circle.members.map((circleMember) => circleMember.user),
        feedItem
      );
    } else if (payload.responding_to_feed_item_id) {
      const respondingToFeedItem = await this.findById(payload.responding_to_feed_item_id);
      // Responding to question
      if (
        payload.type === FeedItemTypeEnum.STORY &&
        respondingToFeedItem.type === FeedItemTypeEnum.QUESTION &&
        payload.responding_to_feed_item_id
      ) {
        await this.notificationService.questionRespondedNotification(user, respondingToFeedItem.author, feedItem);
      }
      // Someone responded to a story that the user created
      if (
        payload.type === FeedItemTypeEnum.STORY &&
        respondingToFeedItem.type === FeedItemTypeEnum.STORY &&
        payload.responding_to_feed_item_id
      ) {
        await this.notificationService.storyRespondedNotification(user, respondingToFeedItem.author, feedItem);
      }
    }
    // Someone sent the user a story to only them and it is not responding to another story or question..
    else if (
      payload.type === FeedItemTypeEnum.STORY &&
      !payload.responding_to_feed_item_id &&
      Array.isArray(payload.shared_with) &&
      payload.shared_with.length === 1
    ) {
      await this.notificationService.storyToldNotification(user, await this.userService.findById(payload.shared_with[0]), feedItem);
    }
    // Someone shared a story with one or individuals
    else if (
      payload.type === FeedItemTypeEnum.STORY &&
      !payload.responding_to_feed_item_id &&
      Array.isArray(payload.shared_with) &&
      payload.shared_with.length > 1
    ) {
      await this.notificationService.storyShared(
        user,
        await Promise.all(payload.shared_with.map((userId: string) => this.userService.findById(userId))),
        feedItem
      );
    } else if (payload.type === FeedItemTypeEnum.STORY && payload.visibility === FeedItemVisibilityEnum.PUBLIC) {
      // Someone shared a story with the whole circle
      const circle = await this.circleService.getByUserId(user.id, [CircleRelationsEnum.MEMBERS, CircleRelationsEnum.MEMBER_USER]);
      await this.notificationService.storyShared(
        user,
        circle.members.map((circleMember) => circleMember.user),
        feedItem
      );
    }
  }

  async isUserAuthorizedToSeeFeedIte(feedItem: FeedItemEntity, userId: string): Promise<boolean> {
    // Users are always authorized for their own feed items
    if (feedItem.author_id === userId) return true;
    // find author's circle
    const circle = await this.circleService.getByUserId(feedItem.author_id);
    if (!circle) return false;
    // find author circle's members
    const circleMember = await this.circleService.getCircleMemberByCircleIdAndMemberId(circle.id, userId);
    if (!circleMember) return false;
    switch (feedItem.visibility) {
      case FeedItemVisibilityEnum.PRIVATE:
        return false;
      case FeedItemVisibilityEnum.PUBLIC:
        return true;
      case FeedItemVisibilityEnum.LIMITED:
        return feedItem.shared_with.some((sharedWith) => sharedWith.user_id === userId);
    }
  }

  async findByIdAndCheckIfUserIsAuthorized(id: string, userId: string): Promise<FeedItemEntity> {
    const feedItem = await this.feedItemRepository
      .createQueryBuilder('feed_items')
      .leftJoinAndSelect('feed_items.media', 'media')
      .leftJoinAndSelect('feed_items.author', 'author')
      .leftJoinAndSelect('feed_items.shared_with', 'shared_with')
      .leftJoinAndSelect('shared_with.user', 'shared_with.user')
      .leftJoinAndSelect('feed_items.responding_feed_items', 'responding_feed_items')
      .leftJoinAndSelect('responding_feed_items.responding_feed_items', 'responding_feed_items.responding_feed_items')
      .where('feed_items.id = :id', { id })
      .orderBy({
        'responding_feed_items.updated_at': 'DESC',
      })
      .getOne();

    if (!feedItem) {
      throw new NotFoundException();
    }

    // check if user is authorized to fetch the given feed item
    // if (!(await this.isUserAuthorizedToSeeFeedIte(feedItem, userId))) {
    //   throw new NotFoundException();
    // }

    // save feed item seen
    // @TODO handle this using events once it's ready
    await this.feedItemSeenService.save(userId, id);

    return {
      ...feedItem,
      responding_feed_items: await Promise.all(
        feedItem.responding_feed_items.map((respondingFeedItem) => {
          return this.findById(respondingFeedItem.id, [FeedItemRelationsEnum.RESPONDING_FEED_ITEMS]);
        })
      ),
      // return shared_with only to the feed item's author
      shared_with: feedItem.author_id === userId ? feedItem.shared_with : undefined,
    };
  }

  async getCreatedFeedItemsByFeedItemType(
    userId: string,
    feedItemType: FeedItemTypeEnum,
    pagination: PaginationQueryRequestDto
  ): Promise<FeedItemsResponseDto> {
    const page = Number(pagination?.page ?? 1);
    const perPage = Number(pagination?.per_page ?? 20);
    const [feedItems, count] = await this.feedItemRepository.findAndCount({
      where: {
        author_id: userId,
        type: feedItemType,
      },
      relations: [FeedItemRelationsEnum.MEDIA, FeedItemRelationsEnum.RESPONDING_FEED_ITEMS],
      take: perPage,
      skip: (page - 1) * perPage,
      order: { created_at: 'DESC' },
    });

    return {
      feed_items: feedItems,
      pagination: getPagination(page, perPage, count),
    };
  }

  async getReceivedFeedItemsByFeedItemType(
    userId: string,
    feedItemType: FeedItemTypeEnum,
    pagination: PaginationQueryRequestDto
  ): Promise<FeedItemsResponseDto> {
    const page = Number(pagination?.page ?? 1);
    const perPage = Number(pagination?.per_page ?? 20);
    const circleMembers = await this.circleService.getAllCircleMembers(userId);
    const circleMemberIds = circleMembers.map((member) => member.id);

    const [feedItems, count] = await this.feedItemRepository
      .createQueryBuilder('feed_items')
      .leftJoinAndSelect('feed_items.author', 'author')
      .leftJoinAndSelect('feed_items.shared_with', 'shared_with')
      .leftJoinAndSelect('feed_items.responding_feed_items', 'responding_feed_items')
      .leftJoinAndSelect('feed_items.seens', 'seens')
      .leftJoinAndSelect('feed_items.media', 'media')
      .andWhere({
        author_id: In(circleMemberIds),
        type: feedItemType,
      })
      .orWhere({
        author_id: In(circleMemberIds),
        visibility: FeedItemVisibilityEnum.PUBLIC,
        type: feedItemType,
      })
      .take(perPage)
      .skip((page - 1) * perPage)
      .orderBy('feed_items.created_at', 'DESC')
      .getManyAndCount();

    return {
      feed_items: feedItems,
      pagination: getPagination(page, perPage, count),
    };
  }

  async getProfileFeed(userId: string, pagination: PaginationQueryRequestDto): Promise<FeedItemsResponseDto> {
    const page = Number(pagination?.page ?? 1);
    const perPage = Number(pagination?.per_page ?? 20);
    const [feedItems, count] = await this.feedItemRepository.findAndCount({
      where: {
        author_id: userId,
        responding_to_feed_item_id: null,
      },
      relations: [FeedItemRelationsEnum.MEDIA, FeedItemRelationsEnum.RESPONDING_FEED_ITEMS],
      take: perPage,
      skip: (page - 1) * perPage,
      order: { created_at: 'DESC' },
    });

    return {
      feed_items: feedItems,
      pagination: getPagination(page, perPage, count),
    };
  }

  async getHomeFeed(userId: string, pagination: PaginationQueryRequestDto): Promise<FeedItemsResponseDto> {
    const page = Number(pagination?.page ?? 1);
    const perPage = Number(pagination?.per_page ?? 20);
    const circleMembers = await this.circleService.getAllCircleMembers(userId);
    const circleMemberIds = circleMembers.map((member) => member.id);
    const [feedItems, count] = await this.feedItemRepository
      .createQueryBuilder('feed_items')
      .leftJoinAndSelect('feed_items.author', 'author')
      .leftJoinAndSelect('author.profile_photo', 'profile_photo')
      .leftJoinAndSelect('feed_items.responding_feed_items', 'responding_feed_items')
      .leftJoinAndSelect('feed_items.responding_to_feed_item', 'responding_to_feed_item')
      .leftJoinAndSelect('feed_items.media', 'media')
      .leftJoinAndSelect('feed_items.shared_with', 'shared_with')
      .leftJoinAndSelect('feed_items.seens', 'seens')
      .leftJoinAndSelect('responding_feed_items.responding_feed_items', 'responding_feed_items.responding_feed_items')

      .where('shared_with.user_id = :uid', { uid: userId })
      .andWhere({
        visibility: FeedItemVisibilityEnum.LIMITED,
        responding_to_feed_item_id: IsNull(),
      })
      .orWhere({
        author_id: In(circleMemberIds),
        visibility: FeedItemVisibilityEnum.PUBLIC,
        responding_to_feed_item_id: IsNull(),
      })
      .orWhere({
        author_id: userId,
        responding_to_feed_item_id: IsNull(),
      })
      .orWhere({
        author_id: In(circleMemberIds),
        responding_to_feed_item: {
          author_id: Not(In([userId, ...circleMemberIds])),
        },
      })
      .take(perPage)
      .skip((page - 1) * perPage)
      .orderBy('feed_items.updated_at', 'DESC')
      .getManyAndCount();

    return {
      feed_items: feedItems,
      pagination: getPagination(page, perPage, count),
    };
  }

  async getHomeFeedFromTrustedUsersOnly(userId: string, pagination: PaginationQueryRequestDto): Promise<FeedItemsResponseDto> {
    const page = Number(pagination?.page ?? 1);
    const perPage = Number(pagination?.per_page ?? 20);
    const circle = await this.circleService.getByUserId(userId, [CircleRelationsEnum.MEMBERS]);
    const circleMemberIds = circle.members.filter((member) => member.are_both_trusted).map((member) => member.member_id);
    const [feedItems, count] = await this.feedItemRepository
      .createQueryBuilder('feed_items')
      .leftJoinAndSelect('feed_items.author', 'author')
      .leftJoinAndSelect('author.profile_photo', 'profile_photo')
      .leftJoinAndSelect('feed_items.shared_with', 'shared_with')
      .leftJoinAndSelect('feed_items.responding_feed_items', 'responding_feed_items')
      .leftJoinAndSelect('feed_items.responding_to_feed_item', 'responding_to_feed_item')
      .leftJoinAndSelect('feed_items.media', 'media')
      .leftJoinAndSelect('feed_items.seens', 'seens')
      .where('shared_with.user_id = :uid', { uid: userId })
      .andWhere({
        author_id: In(circleMemberIds),
        responding_to_feed_item_id: IsNull(),
      })
      .orWhere({
        author_id: In(circleMemberIds),
        visibility: FeedItemVisibilityEnum.PUBLIC,
        responding_to_feed_item_id: IsNull(),
      })
      .orWhere({
        author_id: userId,
        responding_to_feed_item_id: IsNull(),
      })
      .orWhere({
        author_id: In(circleMemberIds),
        responding_to_feed_item: {
          author_id: Not(In([userId, ...circleMemberIds])),
        },
      })
      .take(perPage)
      .skip((page - 1) * perPage)
      .orderBy('feed_items.updated_at', 'DESC')
      .getManyAndCount();

    return {
      feed_items: feedItems,
      pagination: getPagination(page, perPage, count),
    };
  }

  async delete(id: string, userId: string): Promise<void> {
    const feedItem = await this.findById(id);
    if (!feedItem) {
      throw new NotFoundException();
    }
    // check if user is authorized to fetch the given feed item
    if (!(await this.isUserAuthorizedToSeeFeedIte(feedItem, userId))) {
      throw new NotFoundException();
    }
    await this.feedItemRepository.softDelete({
      id,
    });
  }

  async getNumberOfCreatedQuestionsByUserId(userId: string): Promise<number> {
    return this.feedItemRepository.count({
      author_id: userId,
      type: FeedItemTypeEnum.QUESTION,
    });
  }
}
