import { InjectRepository } from '@nestjs/typeorm';
import { Injectable } from '@nestjs/common';

import { FeedItemSeenEntity } from '../entity/feed-item-seen.entity';
import { FeedItemSeenRepository } from './feed-item-seen.repository';

@Injectable()
export class FeedItemSeenService {
  constructor(
    @InjectRepository(FeedItemSeenEntity)
    private readonly feedItemSeenRepository: FeedItemSeenRepository
  ) {}

  async save(userId: string, feedItemId: string): Promise<void> {
    await this.feedItemSeenRepository.save({
      user_id: userId,
      feed_item_id: feedItemId,
    });
  }

  async getLastByFeedIteId(id: string): Promise<FeedItemSeenEntity | null> {
    return this.feedItemSeenRepository.findOne({ id });
  }
}
