import { ConfigService } from '@nestjs/config';
import { InjectRepository } from '@nestjs/typeorm';
import { Injectable, NestMiddleware } from '@nestjs/common';
import { NextFunction, Response } from 'express';
import { verify } from 'jsonwebtoken';

import { EnvVariableNameEnum } from '../common/enums/env-variable-name.enum';
import { RequestWithUser } from '../common/interfaces/request-with-user.interface';
import { UserEntity } from '../entity/user.entity';
import { UserRepository } from '../user/user.repository';

@Injectable()
export class AuthMiddleware implements NestMiddleware {
  constructor(
    @InjectRepository(UserEntity)
    private readonly userRepository: UserRepository,
    private readonly configService: ConfigService
  ) {}

  use(req: RequestWithUser, res: Response, next: NextFunction) {
    try {
      const authorizationHeader = req.headers.authorization;
      if (!authorizationHeader) {
        return res.sendStatus(401);
      }
      const token = authorizationHeader.split(' ')[1];
      if (!token) {
        return res.sendStatus(401);
      }

      verify(token, this.configService.get<string>(EnvVariableNameEnum.JWT_SECRET, ''), async (err, payload) => {
        if (err || payload === undefined) {
          return res.sendStatus(401);
        }
        const user = await this.userRepository.findOne({
          id: payload.id,
        });
        if (user) {
          req.user = user;
          next();
          return;
        }
        return res.sendStatus(401);
      });
    } catch (e: any) {
      return res.sendStatus(401);
    }
  }
}
