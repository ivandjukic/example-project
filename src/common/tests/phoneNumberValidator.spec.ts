import { isPhoneNumberValid } from '../phoneNumberValidator';

describe('phoneNumberValidator', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should return true', () => {
    const result = isPhoneNumberValid('+1 (513) 227-5100');
    expect(result).toBe(true);
  });
  it('should return true', () => {
    const result = isPhoneNumberValid('+1(513)227-5100');
    expect(result).toBe(true);
  });
  it('should return true', () => {
    const result = isPhoneNumberValid('+1513-227-5100');
    expect(result).toBe(true);
  });
  it('should return true', () => {
    const result = isPhoneNumberValid('+15132275100');
    expect(result).toBe(true);
  });
  it('should return true', () => {
    const result = isPhoneNumberValid('+1 5132275100');
    expect(result).toBe(true);
  });
});
