export enum FeedItemRelationsEnum {
  AUTHOR = 'author',
  MEDIA = 'media',
  RESPONDING_TO_FEED_ITEM = 'responding_to_feed_item',
  SHARED_WITH = 'shared_with',
  SEENS = 'seens',
  SHARED_WITH_USER = 'shared_with.user',
  RESPONDING_FEED_ITEMS = 'responding_feed_items',
  RESPONDING_FEED_ITEMS_MEDIA = 'responding_feed_items.media',
}
