export enum CircleRelationsEnum {
  MEMBERS = 'members',
  MEMBER_USER = 'members.user',
}
