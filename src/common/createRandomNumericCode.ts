export const createRandomNumericCode = (length = 4) => {
  let code = '';
  const characters = '0123456789';
  // eslint-disable-next-line no-plusplus
  for (let i = 0; i < length; i++) {
    code += characters.charAt(Math.floor(Math.random() * characters.length));
  }
  return code;
};
