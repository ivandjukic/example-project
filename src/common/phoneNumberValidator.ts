import parsePhoneNumber from 'libphonenumber-js';

export const isPhoneNumberValid = (phoneNumber: string): boolean => {
  const parsedPhoneNumber = parsePhoneNumber(phoneNumber);
  return parsedPhoneNumber !== undefined && parsedPhoneNumber.isValid();
};
