import { ApiProperty } from '@nestjs/swagger';
import { IsNumber, IsOptional } from 'class-validator';
import { Transform } from 'class-transformer';

export class PaginationQueryRequestDto {
  @ApiProperty({
    name: 'page',
    type: 'number',
    example: 1,
    required: false,
    default: 1,
  })
  @Transform(({ value }) => Number(value))
  @IsOptional()
  @IsNumber()
  page: number;

  @ApiProperty({
    name: 'per_page',
    type: 'number',
    example: 20,
    required: false,
    default: 20,
  })
  @Transform(({ value }) => Number(value))
  @IsOptional()
  @IsNumber()
  per_page: number;
}
