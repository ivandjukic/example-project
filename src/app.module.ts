import * as winston from 'winston';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { TwilioModule } from 'nestjs-twilio';
import { TypeOrmModule } from '@nestjs/typeorm';
import { WinstonModule } from 'nest-winston';

import { AppEnbConfig, ConfigValue, EnvVariableNameEnum } from './common/enums/env-variable-name.enum';
import { AuthMiddleware } from './middleware/auth.middleware';
import { AuthenticationModule } from './authentication/authentication.module';
import { CircleEntity } from './entity/circle.entity';
import { CircleMemberEntity } from './entity/circleMember.entity';
import { CircleModule } from './circle/circle.module';
import { CountryCodesModule } from './country-codes/country-codes.module';
import { CreatorModule } from './creator/creator.module';
import { DeviceTokenEntity } from './entity/device-token.entity';
import { DeviceTokenModule } from './device-token/device-token.module';
import { FeedItemEntity } from './entity/feed-item.entity';
import { FeedItemModule } from './feed-item/feed-item.module';
import { FeedItemSeenEntity } from './entity/feed-item-seen.entity';
import { FeedItemShareEntity } from './entity/feed-item-share.entity';
import { FeedbackEntity } from './entity/feedback.entity';
import { FeedbackModule } from './feedback/feedback.module';
import { InviteEntity } from './entity/invite.entity';
import { InviteModule } from './invite/invite.module';
import { ListenEntity } from './entity/listen.entity';
import { ListenModule } from './listen/listen.module';
import { ListenerEntity } from './entity/listener.entity';
import { ListenerModule } from './listener/listener.module';
import { LoginEntity } from './entity/login.entity';
import { LoginModule } from './login/login.module';
import { OtpCodeEntity } from './entity/otpCode.entity';
import { OtpCodeModule } from './otp-code/otp-code.module';
import { ProfilePhotoEntity } from './entity/profile-photo.entity';
import { ProfilePhotoModule } from './profile-photo/profile-photo.module';
import { ServerModule } from './server/server.module';
import { SettingsEntity } from './entity/settings.entity';
import { SettingsModule } from './settings/settings.module';
import { TermsAndConditionsModule } from './terms-and-conditions/terms-and-conditions.module';
import { UploadEntity } from './entity/upload.entity';
import { UploadModule } from './upload/upload.module';
import { UserEntity } from './entity/user.entity';
import { UserModule } from './user/user.module';
import { WaitlistEntity } from './entity/waitlist.entity';
import { WaitlistModule } from './waitlist/waitlist.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    TypeOrmModule.forRootAsync({
      inject: [ConfigService],
      useFactory: (config: ConfigService) => {
        return {
          type: 'postgres',
          host: config.get<string>('POSTGRES_HOST', '127.0.0.1'),
          port: config.get<number>('POSTGRES_PORT', 5432),
          username: config.get<string>('POSTGRES_USER', 'trust'),
          password: config.get<string>('POSTGRES_PASSWORD', 'trust'),
          database: config.get<string>('POSTGRES_DATABASE', 'trust'),
          entities: [
            SettingsEntity,
            UserEntity,
            InviteEntity,
            CircleEntity,
            CircleMemberEntity,
            OtpCodeEntity,
            DeviceTokenEntity,
            FeedbackEntity,
            WaitlistEntity,
            UploadEntity,
            FeedItemEntity,
            FeedItemShareEntity,
            ProfilePhotoEntity,
            FeedItemSeenEntity,
            LoginEntity,
            ListenEntity,
            ListenerEntity,
          ],
          synchronize: config.get<ConfigValue>(EnvVariableNameEnum.RUN_MIGRATIONS, ConfigValue.ENABLED) === ConfigValue.ENABLED,
          logging: config.get<AppEnbConfig>(EnvVariableNameEnum.ENV, AppEnbConfig.LOCAL) !== AppEnbConfig.PROD,
        };
      },
    }),
    TypeOrmModule.forFeature([UserEntity]),
    TwilioModule.forRootAsync({
      inject: [ConfigService],
      useFactory: (config: ConfigService) => {
        return {
          accountSid: config.get<string>(EnvVariableNameEnum.TWILIO_SID, ''),
          authToken: config.get<string>(EnvVariableNameEnum.TWILIO_TOKEN, ''),
        };
      },
    }),
    WinstonModule.forRootAsync({
      useFactory: () => ({
        level: 'debug',
        transports: [
          new winston.transports.Console(),
          new winston.transports.File({
            filename: 'logs/app.log',
          }),
        ],
        format: winston.format.combine(
          winston.format.timestamp({
            format: 'MMM-DD-YYYY HH:mm:ss',
          }),
          winston.format.printf((info) => {
            return `${info.level}: ${[info.timestamp]}: ${JSON.stringify({
              context: info.context,
              message: info.message,
              error: info.error,
            })}`;
          })
        ),
      }),
      inject: [],
    }),
    ServerModule,
    TermsAndConditionsModule,
    SettingsModule,
    AuthenticationModule,
    InviteModule,
    CircleModule,
    OtpCodeModule,
    UserModule,
    DeviceTokenModule,
    FeedbackModule,
    WaitlistModule,
    CountryCodesModule,
    UploadModule,
    FeedItemModule,
    ProfilePhotoModule,
    LoginModule,
    ListenModule,
    ListenerModule,
    CreatorModule,
  ],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(AuthMiddleware)
      .exclude('invites/:phone_number/is-invited', '/invites/accept')
      .forRoutes(
        'users',
        'device-tokens',
        'feedbacks',
        'invites',
        'circles',
        'uploads',
        'feed-items',
        'profile-photos',
        'listens',
        'listeners',
        'creators'
      );
  }
}
