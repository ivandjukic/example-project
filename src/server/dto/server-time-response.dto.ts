import { ApiProperty } from '@nestjs/swagger';

export class ServerTimeResponseDto {
  @ApiProperty({
    type: 'number',
    name: 'time',
  })
  time: number;
}
