import { ApiProperty } from '@nestjs/swagger';

export class ServerStatusResponseDto {
  @ApiProperty({
    type: 'string',
    name: 'status',
  })
  status: 'ok';
}
