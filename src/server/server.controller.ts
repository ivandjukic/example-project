import { ApiBearerAuth, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Controller, Get, HttpStatus } from '@nestjs/common';

import { ServerStatusResponseDto } from './dto/server-status-response.dto';
import { ServerTimeResponseDto } from './dto/server-time-response.dto';
import { SwaggerTags } from '../common/enums/swagger-tags.enum';

@ApiTags(SwaggerTags.SERVER)
@Controller('')
export class ServerController {
  @Get('/')
  @ApiOperation({ summary: 'Get server status' })
  @ApiBearerAuth()
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Successfully retrieved server status',
    type: ServerStatusResponseDto,
  })
  GetServerStatus(): any {
    return {
      status: 'ok',
    };
  }

  @Get('/time')
  @ApiOperation({ summary: "Get server's time" })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Returns current server time (unix epoch timestamp)',
    type: ServerTimeResponseDto,
  })
  GetServerTime(): ServerTimeResponseDto {
    return {
      time: Date.now(),
    };
  }
}
