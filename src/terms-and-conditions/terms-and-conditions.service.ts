import { InjectRepository } from '@nestjs/typeorm';
import { Injectable } from '@nestjs/common';

import { SettingKey } from '../settings/enum/settings-key.enum';
import { SettingsEntity } from '../entity/settings.entity';
import { SettingsRepository } from '../settings/settings.repository';

@Injectable()
export class TermsAndConditionsService {
  constructor(
    @InjectRepository(SettingsEntity)
    private readonly settingsRepository: SettingsRepository
  ) {}

  async getTermsAndConditions(): Promise<string> {
    const settings = await this.settingsRepository.findOne({
      key: SettingKey.TERMS_AND_CONDITIONS,
    });
    return settings?.value ?? '';
  }
}
