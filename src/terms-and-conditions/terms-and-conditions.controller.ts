import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Controller, Get, HttpStatus } from '@nestjs/common';

import { SwaggerTags } from '../common/enums/swagger-tags.enum';
import { TermsAndConditionsResponseDto } from './dto/terms-and-conditions-response.dto';
import { TermsAndConditionsService } from './terms-and-conditions.service';

@ApiTags(SwaggerTags.TERMS_AND_CONDITIONS)
@Controller('terms-and-conditions')
export class TermsAndConditionsController {
  constructor(private readonly termsAndConditionsService: TermsAndConditionsService) {}

  @Get()
  @ApiOperation({ summary: 'Get T&C' })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Successfully retrieved terms and conditions',
    type: TermsAndConditionsResponseDto,
  })
  async getTermsAndConditions() {
    return {
      terms_and_conditions: await this.termsAndConditionsService.getTermsAndConditions(),
    };
  }
}
