import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { SettingsEntity } from '../entity/settings.entity';
import { TermsAndConditionsController } from './terms-and-conditions.controller';
import { TermsAndConditionsService } from './terms-and-conditions.service';

@Module({
  imports: [TypeOrmModule.forFeature([SettingsEntity])],
  providers: [TermsAndConditionsService],
  controllers: [TermsAndConditionsController],
})
export class TermsAndConditionsModule {}
