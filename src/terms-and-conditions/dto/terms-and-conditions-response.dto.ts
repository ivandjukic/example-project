import { ApiProperty } from '@nestjs/swagger';

export class TermsAndConditionsResponseDto {
  @ApiProperty({
    type: 'string',
    name: 'terms_and_conditions',
  })
  terms_and_conditions: string;
}
